DROP DATABASE IF EXISTS `cwc`;
CREATE DATABASE `cwc`;
USE `cwc`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: cwc
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cwc_activity`
--

DROP TABLE IF EXISTS `cwc_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cwc_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `complaint_id` int(11) DEFAULT NULL,
  `reporter_id` int(11) DEFAULT NULL,
  `assignee_id` int(11) DEFAULT NULL,
  `comment` varchar(1024) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`activity_id`),
  KEY `activity_complaint_id_fk_idx` (`complaint_id`),
  KEY `reporter_id_fk_idx` (`reporter_id`),
  KEY `assignee_id_idx` (`assignee_id`),
  KEY `status_fk_idx` (`status`),
  KEY `updated_by_fk_idx` (`updated_by`),
  CONSTRAINT `activity_complaint_id_fk` FOREIGN KEY (`complaint_id`) REFERENCES `cwc_complaint` (`complaint_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `assignee_id_fk` FOREIGN KEY (`assignee_id`) REFERENCES `cwc_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `reporter_id_fk` FOREIGN KEY (`reporter_id`) REFERENCES `cwc_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `status_fk` FOREIGN KEY (`status`) REFERENCES `cwc_status` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `updated_by_fk` FOREIGN KEY (`updated_by`) REFERENCES `cwc_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cwc_activity`
--

LOCK TABLES `cwc_activity` WRITE;
/*!40000 ALTER TABLE `cwc_activity` DISABLE KEYS */;
INSERT INTO `cwc_activity` VALUES (17,9,1,1,'','2016-03-06 17:26:11','PENDING',1),(18,10,1,1,'','2016-03-06 17:27:39','PENDING',1),(19,11,1,1,'','2016-03-06 22:09:19','PENDING',1),(20,12,1,1,'','2016-03-06 22:16:43','PENDING',1),(21,13,1,1,'','2016-03-06 22:34:49','PENDING',1),(22,12,1,2,'Please look into it.','2016-03-06 23:22:55','ASSIGNED',1);
/*!40000 ALTER TABLE `cwc_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cwc_category`
--

DROP TABLE IF EXISTS `cwc_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cwc_category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL DEFAULT 'General',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cwc_category`
--

LOCK TABLES `cwc_category` WRITE;
/*!40000 ALTER TABLE `cwc_category` DISABLE KEYS */;
INSERT INTO `cwc_category` VALUES (1,'General','General'),(2,'Magistrate','SuperAdmin'),(3,'Inspector','Admin'),(4,'Sub Inspector','Admin'),(5,'SP','Admin'),(6,'DSP','Admin'),(7,'DCP','Admin');
/*!40000 ALTER TABLE `cwc_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cwc_complaint`
--

DROP TABLE IF EXISTS `cwc_complaint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cwc_complaint` (
  `complaint_id` int(11) NOT NULL AUTO_INCREMENT,
  `identity_id` int(11) DEFAULT NULL,
  `identity_number` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  `place` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `subject` varchar(45) NOT NULL,
  `description` varchar(45) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'pending',
  `complaint_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`complaint_id`),
  KEY `identity_id_idx` (`identity_id`),
  KEY `complaint_user_id_fk_idx` (`complaint_user_id`),
  CONSTRAINT `complaint_user_id_fk` FOREIGN KEY (`complaint_user_id`) REFERENCES `cwc_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `identity_id_fk` FOREIGN KEY (`identity_id`) REFERENCES `cwc_identity` (`IDENTITY_ID`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cwc_complaint`
--

LOCK TABLES `cwc_complaint` WRITE;
/*!40000 ALTER TABLE `cwc_complaint` DISABLE KEYS */;
INSERT INTO `cwc_complaint` VALUES (9,2,'123123','2016-03-06 00:00:00','Gariahat','Kolkata','Book Stolen','Book Stolen','pending',4),(10,4,'123','2015-02-01 00:00:00','Baguiati','Kolkata','Watch','Watch Stolen','pending',4),(11,3,'345123','2016-01-01 00:00:00','Airport','Kolkata','Computer','Computer stolen','pending',4),(12,2,'11156','2016-02-04 00:00:00','SDF','Saltlake','Mobile','Mobile Stolen','pending',3),(13,5,'116789','2016-02-03 00:00:00','New Market','Kolkata','Wrong Product','Wrong Product was delivered','pending',1);
/*!40000 ALTER TABLE `cwc_complaint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cwc_identity`
--

DROP TABLE IF EXISTS `cwc_identity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cwc_identity` (
  `IDENTITY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`IDENTITY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cwc_identity`
--

LOCK TABLES `cwc_identity` WRITE;
/*!40000 ALTER TABLE `cwc_identity` DISABLE KEYS */;
INSERT INTO `cwc_identity` VALUES (1,'PASSPORT'),(2,'VOTER ID CARD'),(3,'AADHAR CARD'),(4,'PAN CARD'),(5,'DRIVING LICENSE');
/*!40000 ALTER TABLE `cwc_identity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cwc_status`
--

DROP TABLE IF EXISTS `cwc_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cwc_status` (
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cwc_status`
--

LOCK TABLES `cwc_status` WRITE;
/*!40000 ALTER TABLE `cwc_status` DISABLE KEYS */;
INSERT INTO `cwc_status` VALUES ('ASSIGNED','An officer has been assigned.'),('CLOSED','The case has been closed.'),('IN PROGRESS','The matter is being looked at.'),('PENDING','No action has been taken till now'),('RE-OPENED','The work on this issue has been resumed'),('RESOLVED','The matter has been resolved.'),('STARTED','The work has been initiated'),('STOPPED','All activities have been stopped.');
/*!40000 ALTER TABLE `cwc_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cwc_user`
--

DROP TABLE IF EXISTS `cwc_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cwc_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `user_category_id` int(11) DEFAULT '1',
  `status` varchar(45) NOT NULL DEFAULT 'NORMAL',
  PRIMARY KEY (`user_id`),
  KEY `user_category_id_pk_idx` (`user_category_id`),
  CONSTRAINT `user_category_id_pk` FOREIGN KEY (`user_category_id`) REFERENCES `cwc_category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cwc_user`
--

LOCK TABLES `cwc_user` WRITE;
/*!40000 ALTER TABLE `cwc_user` DISABLE KEYS */;
INSERT INTO `cwc_user` VALUES (1,'aks','123','Akanksha Shinde','Calcutta','9433148573','aks@gmail.com','female',2,'NORMAL'),(2,'sur','123','Surya Shinde','Mumbai','9876123123','sur@gmail.com','male',3,'NORMAL'),(3,'rudra','123','Rudra Pal','Ghatal','9878123123','rud@gmail.com','male',4,'NORMAL'),(4,'sanz','123','Sanjeev Saha','Howrah','9433148576','sanzibb@gmail.com','male',1,'NORMAL'),(5,'kan','123','Kanhaiya Kumar','Delhi','9433123123','kan@yahoo.com','male',3,'NORMAL'),(6,'virat','123','Virat Kohli','Delhi','9433123123','virat@gmail.com','male',1,'NORMAL'),(7,'dhoni','123','M. Dhoni','Ranchi','9648123123','dhoni@gmail.com','male',1,'NORMAL'),(8,'sach','123','Sachin Ten','Mumbai','9876123123','sach@yahoo.com','male',4,'CANCEL'),(9,'kris','123','Kris R','Belurmath','9433123123','kris@gmail.com','male',2,'NORMAL');
/*!40000 ALTER TABLE `cwc_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-09  5:30:33
