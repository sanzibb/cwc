--
-- This is dump for Postgres database
--

DROP TABLE IF EXISTS cwc_identity;

CREATE SEQUENCE cwc_identity_seq;
CREATE TABLE cwc_identity (
  IDENTITY_ID INTEGER NOT NULL DEFAULT NEXTVAL('cwc_identity_seq'),
  NAME TEXT NOT NULL,
  PRIMARY KEY (IDENTITY_ID)
);

--
-- Dumping data for table cwc_identity
--

INSERT INTO cwc_identity VALUES (1,'PASSPORT'),(2,'VOTER ID CARD'),(3,'AADHAR CARD'),(4,'PAN CARD'),(5,'DRIVING LICENSE');

--
-- Table structure for table 'cwc_status'
--

DROP TABLE IF EXISTS cwc_status;

CREATE TABLE cwc_status (
  name TEXT NOT NULL,
  description TEXT DEFAULT NULL,
  PRIMARY KEY (name)
);

--
-- Dumping data for table 'cwc_status'
--

INSERT INTO cwc_status VALUES ('ASSIGNED','An officer has been assigned.'),('CLOSED','The case has been closed.'),('IN PROGRESS','The matter is being looked at.'),('PENDING','No action has been taken till now'),('RE-OPENED','The work on this issue has been resumed'),('RESOLVED','The matter has been resolved.'),('STARTED','The work has been initiated'),('STOPPED','All activities have been stopped.');

--
-- Table structure for table cwc_category
--

DROP TABLE IF EXISTS cwc_category;
CREATE SEQUENCE cwc_category_seq;
CREATE TABLE cwc_category (
  category_id INTEGER NOT NULL  DEFAULT NEXTVAL('cwc_category_seq'),
  name TEXT NOT NULL,
  type TEXT NOT NULL DEFAULT 'General',
  PRIMARY KEY (category_id)
);

--
-- Dumping data for table cwc_designation
--
INSERT INTO cwc_category VALUES (1,'General','General'),(2,'Magistrate','SuperAdmin'),(3,'Inspector','Admin'),(4,'Sub Inspector','Admin'),(5,'SP','Admin'),(6,'DSP','Admin'),(7,'DCP','Admin');


--
-- Table structure for table cwc_user
--
DROP TABLE IF EXISTS cwc_user;

CREATE SEQUENCE cwc_user_seq;

CREATE TABLE cwc_user (
  user_id INTEGER NOT NULL DEFAULT NEXTVAL('cwc_user_seq'),
  username TEXT NOT NULL,
  password TEXT NOT NULL,
  name TEXT NOT NULL,
  address TEXT NOT NULL,
  phone TEXT NOT NULL,
  email TEXT DEFAULT NULL,
  gender TEXT NOT NULL,
  user_category_id INTEGER NOT NULL,
  status TEXT NOT NULL DEFAULT 'NORMAL',
  PRIMARY KEY (user_id),  
  CONSTRAINT user_category_id_fk FOREIGN KEY (user_category_id) REFERENCES cwc_category (category_id) ON UPDATE NO ACTION
);


--
-- Dumping data for table cwc_user
--
INSERT INTO cwc_user VALUES (1,'aks','123','Akanksha Shinde','Calcutta','9433148573','aks@gmail.com','female',2,'NORMAL'),(2,'sur','123','Surya Shinde','Mumbai','9876123123','sur@gmail.com','male',3,'NORMAL'),(3,'rudra','123','Rudra Pal','Ghatal','9878123123','rud@gmail.com','male',4,'NORMAL'),(4,'sanz','123','Sanjeev Saha','Howrah','9433148576','sanzibb@gmail.com','male',1,'NORMAL'),(5,'kan','123','Kanhaiya Kumar','Delhi','9433123123','kan@yahoo.com','male',3,'NORMAL'),(6,'virat','123','Virat Kohli','Delhi','9433123123','virat@gmail.com','male',1,'NORMAL'),(7,'dhoni','123','M. Dhoni','Ranchi','9648123123','dhoni@gmail.com','male',1,'NORMAL'),(8,'sach','123','Sachin Ten','Mumbai','9876123123','sach@yahoo.com','male',4,'CANCEL'),(9,'kris','123','Kris R','Belurmath','9433123123','kris@gmail.com','male',2,'NORMAL');


--
-- Table structure for table cwc_complaint
--

DROP TABLE IF EXISTS cwc_complaint;

CREATE SEQUENCE cwc_complaint_seq;
CREATE TABLE cwc_complaint (
  complaint_id INTEGER NOT NULL DEFAULT nextval('cwc_complaint_seq'),
  identity_id INTEGER DEFAULT NULL,
  identity_number TEXT NOT NULL,  
  date TIMESTAMP NOT NULL,
  place TEXT NOT NULL,
  city TEXT NOT NULL,
  subject TEXT NOT NULL,
  description TEXT NOT NULL,
  status TEXT NOT NULL DEFAULT 'pending',
  complaint_user_id INTEGER NOT NULL,  
  PRIMARY KEY (complaint_id), 
  CONSTRAINT identity_id_fk FOREIGN KEY (identity_id) REFERENCES cwc_identity (IDENTITY_ID) ON UPDATE NO ACTION,
  CONSTRAINT complaint_user_id_fk FOREIGN KEY (complaint_user_id) REFERENCES cwc_user (USER_ID) ON UPDATE NO ACTION
);

--
-- Dumping data for table cwc_complaint
--
INSERT INTO cwc_complaint VALUES (9,2,'123123','2016-03-06 00:00:00','Gariahat','Kolkata','Book Stolen','Book Stolen','pending',4),(10,4,'123','2015-02-01 00:00:00','Baguiati','Kolkata','Watch','Watch Stolen','pending',4),(11,3,'345123','2016-01-01 00:00:00','Airport','Kolkata','Computer','Computer stolen','pending',4),(12,2,'11156','2016-02-04 00:00:00','SDF','Saltlake','Mobile','Mobile Stolen','pending',3),(13,5,'116789','2016-02-03 00:00:00','New Market','Kolkata','Wrong Product','Wrong Product was delivered','pending',1);

--
-- Table structure for table cwc_activity
--

DROP TABLE IF EXISTS cwc_activity;

CREATE SEQUENCE cwc_activity_seq;
CREATE TABLE cwc_activity (
  activity_id INTEGER NOT NULL DEFAULT NEXTVAL('cwc_activity_seq'),
  complaint_id INTEGER DEFAULT NULL,
  reporter_id INTEGER DEFAULT NULL,
  assignee_id INTEGER DEFAULT NULL,
  comment TEXT DEFAULT NULL,
  date TIMESTAMP DEFAULT NULL,
  status TEXT DEFAULT NULL,
  updated_by INTEGER DEFAULT NULL,
  PRIMARY KEY (activity_id),  
  CONSTRAINT activity_complaint_id_fk FOREIGN KEY (complaint_id) REFERENCES cwc_complaint (complaint_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT assignee_id FOREIGN KEY (assignee_id) REFERENCES cwc_user (user_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT reporter_id_fk FOREIGN KEY (reporter_id) REFERENCES cwc_user (user_id) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT status_fk FOREIGN KEY (status) REFERENCES cwc_status (name) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT updated_by_fk FOREIGN KEY (updated_by) REFERENCES cwc_user (user_id) ON DELETE NO ACTION ON UPDATE NO ACTION
);

--
-- Dumping data for table cwc_activity
--
INSERT INTO cwc_activity VALUES (17,9,1,1,'','2016-03-06 17:26:11','PENDING',1),(18,10,1,1,'','2016-03-06 17:27:39','PENDING',1),(19,11,1,1,'','2016-03-06 22:09:19','PENDING',1),(20,12,1,1,'','2016-03-06 22:16:43','PENDING',1),(21,13,1,1,'','2016-03-06 22:34:49','PENDING',1),(22,12,1,2,'Please look into it.','2016-03-06 23:22:55','ASSIGNED',1);

--
-- Table structure for table cwc_keyvalue
--

DROP TABLE IF EXISTS cwc_keyvalue;

CREATE TABLE cwc_keyvalue (
  key TEXT NOT NULL,
  value TEXT DEFAULT NULL,
  PRIMARY KEY (key)
);

--
-- Dumping data for table cwc_value
--

INSERT INTO cwc_keyvalue VALUES ('CONTACTS',NULL),('ABOUT_US',NULL),('FAQ',NULL);


DROP TABLE IF EXISTS cwc_feedback;

CREATE SEQUENCE cwc_feedback_seq;
CREATE TABLE cwc_feedback (
  feedback_id INTEGER NOT NULL DEFAULT NEXTVAL('cwc_feedback_seq'),
  date TIMESTAMP,
  subject TEXT,
  description TEXT,
  feedback_user_id INTEGER NOT NULL, 
  PRIMARY KEY (feedback_id),
  CONSTRAINT feedback_user_id_fk FOREIGN KEY (feedback_user_id) REFERENCES cwc_user (USER_ID) ON UPDATE NO ACTION
);

