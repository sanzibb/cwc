<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<%
    String cssUrl = (String) application.getAttribute("cssUrl");
%>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->
                    <h3>Search Complaints</h3>
                    <hr>
                    <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  style="width:330px">                           
                        <TR> 
                            <TD colspan="3">&nbsp;</TD>                                    
                        </TR>                               
                        <FORM method="post" action="ControllerServlet"> 
                            <INPUT TYPE="HIDDEN" NAME="action" VALUE="doSearch"/> 
                            <TR> 
                                <TD ALIGN="LEFT">Complaint Number</TD> 
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="TEXT" NAME="complaintId" VALUE="${param.complaintId}">
                                </TD>                                        
                                                                                             
                            </TR>
                            <TR> 
                                <TD colspan="3" align="CENTER"><b>OR</b></TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Keyword</TD> 
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="TEXT" NAME="keyword" VALUE="${param.keyword}">
                                </TD>                                                                                                               
                            </TR>                             
                            <TR>
                                <TD ALIGN="LEFT">&nbsp;</td>
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="RESET" VALUE="Reset" id="form-reset">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <INPUT TYPE="SUBMIT" VALUE="Search" id="form-submit">
                                </TD>
                                
                            </TR>                     
                        </FORM>
                    </TABLE>        
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
