<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<%
    Object obj = session.getAttribute("user");
    User user = obj instanceof User ? (User) obj : null;
   
    String base = (String) application.getAttribute("base");
    String cssUrl = (String) application.getAttribute("cssUrl");
    
    List<Complaint> complaints = dbBean.getComplaints();
    
    List<Complaint> complaintsByUser = new ArrayList<Complaint>();
    
    List<Complaint> complaintsByOthers = new ArrayList<Complaint>();
    
    for(int i=0; i<complaints.size(); i++){
        Complaint co= complaints.get(i);
        if(user!=null && co.getUserId()==user.getUserId()){
            complaintsByUser.add(co);
        }else{
            complaintsByOthers.add(co);
        }
    }
    
    
    
    request.setAttribute("complaints", complaints);
    request.setAttribute("complaintsByUser", complaintsByUser);
    request.setAttribute("complaintsByOthers", complaintsByOthers);
   
    
%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                    <!-- page content starts here -->
                     <h3>Recent Complaints</h3>
                     <br>
                       <% if(user!=null && complaintsByUser.size() !=0){ System.out.println("complaintsByUser: "+complaintsByUser);%> 
                       <h5>Complaints lodged by you</h5>
                       <br>
                       <c:forEach items="${complaintsByUser}" var="complaint" varStatus="counter">
                            <article>
                                <h4>${complaint.subject}</h4>
                                <div class="article-info">
                                    Posted on <strong>${complaint.date}</strong>&nbsp;
                                    by <strong>${dbBean.getUser(complaint.userId).getName()}</strong>&nbsp;
                                    from <strong>${complaint.place},&nbsp;${complaint.city}</strong>
                                </div>
                                <c:choose>
                                    <c:when test="${complaint.description.length() > 120}">
                                        <p>${complaint.description.substring(0, complaint.description.substring(0, 120).lastIndexOf(' '))}&nbsp; <strong>. . .</strong></p>	
                                        
                                    </c:when>
                                    <c:otherwise>
                                        <p>${complaint.description}</p>
                                    </c:otherwise>
                                </c:choose>
                                <span style="float: right">
                                    <a href="<%=base%>?action=displayReadMore&complaintId=${complaint.complaintId}" class="button">Read more</a>
                                </span>        
                            </article>
                        </c:forEach>         
                       
                       <%}%>
                       <% if(user!=null && complaintsByOthers.size() !=0){System.out.println("complaintsByOthers "+complaintsByOthers);%> 
                       <h5>Complaints lodged by others</h5>
                       <br>
                       <c:forEach items="${complaintsByOthers}" var="complaint" varStatus="counter">
                            <article>
                                <h4>${complaint.subject}</h4>
                                <div class="article-info">
                                    Posted on <strong>${complaint.date}</strong>&nbsp;
                                    by <strong>${dbBean.getUser(complaint.userId).getName()}</strong>&nbsp;
                                    from <strong>${complaint.place},&nbsp;${complaint.city}</strong>
                                </div>
                                <c:choose>
                                    <c:when test="${complaint.description.length() > 120}">
                                        <p>${complaint.description.substring(0, complaint.description.substring(0, 120).lastIndexOf(' '))}&nbsp; <strong>. . .</strong></p>	
                                        
                                    </c:when>
                                    <c:otherwise>
                                        <p>${complaint.description}</p>
                                    </c:otherwise>
                                </c:choose>
                                <span style="float: right">
                                    <a href="<%=base%>?action=displayReadMore&complaintId=${complaint.complaintId}" class="button">Read more</a>
                                </span>        
                            </article>
                        </c:forEach>         
                       
                       <%}%>
                       <%if(user==null){%> 
                        <c:forEach items="${complaints}" var="complaint" varStatus="counter">
                            <article>
                                <h4>${complaint.subject}</h4>
                                <div class="article-info">
                                    Posted on <strong>${complaint.date}</strong>&nbsp;
                                    by <strong>${dbBean.getUser(complaint.userId).getName()}</strong>&nbsp;
                                    from <strong>${complaint.place},&nbsp;${complaint.city}</strong>
                                </div>
                                <c:choose>
                                    <c:when test="${complaint.description.length() > 120}">
                                        <p>${complaint.description.substring(0, complaint.description.substring(0, 120).lastIndexOf(' '))}&nbsp; <strong>. . .</strong></p>	
                                        
                                    </c:when>
                                    <c:otherwise>
                                        <p>${complaint.description}</p>
                                    </c:otherwise>
                                </c:choose>
                                <span style="float: right">
                                    <a href="<%=base%>?action=displayReadMore&complaintId=${complaint.complaintId}" class="button">Read more</a>
                                </span>        
                            </article>
                        </c:forEach> 
                        <%}%>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
