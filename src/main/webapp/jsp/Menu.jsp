<%@ page import="com.aks.cwc.model.*" %> 
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/> 
<%
    String base = (String) application.getAttribute("base");
    Object obj = session.getAttribute("user");
    User user = obj instanceof User ? (User) obj : null;
%>
<aside id="sidebar" class="column-left">
    <ul>
        <li>
            <h4>General</h4>
            <ul class="blocklist">
                <li><a href="<%=base%>?action=displayHome>">Recent Complaints</a></li>
                <li><a href="<%=base%>?action=displayComplaintForm" title="Complaints Form">Complaints Form</a></li>              
              
                <%if (user == null) {%>
                <li><a href="<%=base%>?action=displayRegistration&formType=user">User Registration</a></li> 
                <li><a href="<%=base%>?action=displayLogin">Login</a></li>
                <%}else{%>
                <li><a href="<%=base%>?action=doLogout">Logout</a></li>
                <li><a href="<%=base%>?action=changePassword">Change Password</a></li>
                <li><a href="<%=base%>?action=updateProfile">Profile</a></li>
                <%}%>                
            </ul>
        </li>
        <%if (user == null) {%>
        <li>
            <h4>Administrator</h4>
            <ul class="blocklist">  
                <li><a href="<%=base%>?action=displayRegistration&formType=admin">Admin Registration</a></li>     
               
            </ul>
        </li>
        <%}%>                

        <%if (user != null && dbBean.getCategory(user.getCategoryId()).isSuperAdmin()) {%>
        <li>
            <h4>Management</h4>
            <ul>
                <li><a href="<%=base%>?action=displayUpdateContacts&title=Update%20Contacts&key=CONTACTS">Contacts</a></li>
                <li><a href="<%=base%>?action=displayUpdateFaq&title=Update%20FAQ&key=FAQ">FAQ</a></li>
                <li><a href="<%=base%>?action=displayUpdateAboutUs&title=Update%20About%20Us&key=ABOUT_US">About Us</a></li>
                <li><a href="<%=base%>?action=displayUsers">Users</a></li>
            </ul>
        </li>
        <%}%>
        <li>
            <h4>Feedback</h4>
            <ul class="blocklist">
                <li><a href="<%=base%>?action=displayFeedbackForm">Feedback Form</a></li>
                <li><a href="<%=base%>?action=displayFeedbacks">Recent Feedbacks</a></li>
                <%if (user != null && dbBean.getCategory(user.getCategoryId()).isSuperAdmin()) {%>
                <li><a href="<%=base%>?action=displayEditFeedbacks">Edit Feedbacks</a></li>
                <%}%>
            </ul>
        </li>
        
    </ul>
</aside>
