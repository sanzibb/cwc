<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application"  class="com.aks.cwc.model.DbBean"/>
<%
    String cssUrl = (String) application.getAttribute("cssUrl");
    String username = request.getParameter("username");
    String password = request.getParameter("password");

    String nextURL = request.getParameter("nextURL");    
       

    String action = request.getParameter("action");   
     

    if (action != null && action.equals("doLogin")) {
        if (Util.isBlank(username)) {
            request.setAttribute("status", "error");
            request.setAttribute("message", "Username can not be blank.");
        } else if (Util.isBlank(password)) {
            request.setAttribute("status", "error");
            request.setAttribute("message", "Password can not be blank.");
        } else {
            User user = dbBean.doLogin(username, password);
            if (user == null) {
                request.setAttribute("status", "error");
                request.setAttribute("message", "Invalid Username or Password");
            } else {
                if(user.getStatus().equalsIgnoreCase("NORMAL")){
                     request.getSession().setAttribute("user", user);
                     String welcomeURL =null;
                     if(dbBean.getCategory(user.getCategoryId()).isGeneral()){
                        welcomeURL =  "/jsp/Default.jsp";
                     }else{
                        welcomeURL =  "/jsp/AdminTask.jsp";
                     }
                     
                     RequestDispatcher requestDispatcher = null;
                     
                     nextURL = Util.isBlank(nextURL)?welcomeURL:nextURL;                  
                     requestDispatcher = getServletContext().getRequestDispatcher(nextURL);                                     
                     requestDispatcher.forward(request, response);
                }else{
                    request.setAttribute("status", "error");
                    request.setAttribute("message", "You are not allowed to login.<br>&nbsp;&nbsp;&nbsp;&nbsp;Please contact Administrator.");
                }
              
            }
        }
        
    }
    
    String msgColor = request.getAttribute("status")!=null && request.getAttribute("status").toString().equalsIgnoreCase("ERROR")?"red":"green";
    request.setAttribute("msgColor", msgColor);

%>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
               <article>                
                    <!-- page content starts here -->
                    
                    <h3>Login</h3>
                    <hr>
                    <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0" style="width:280px">
                        <TR> 
                            <TD colspan="3">&nbsp;</TD>                                    
                        </TR>                            
                        <FORM method="post" action="ControllerServlet"> 
                            <input type="HIDDEN" name="action" value="doLogin"/>
                            <INPUT TYPE="HIDDEN" NAME="nextURL" VALUE="${param.nextURL!=null?param.nextURL:requestScope.nextURL}"/>                             
                            
                            <TR> 
                                <TD ALIGN="LEFT">Username</td>
                                <TD ALIGN="LEFT" colspan="2"><INPUT TYPE="TEXT" NAME="username" VALUE="${param.username}"></TD>                                                              
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Password</td>
                                <TD ALIGN="LEFT" colspan="2"><INPUT TYPE="PASSWORD" NAME="password"></TD>                                                              
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">&nbsp;</td>
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="RESET" VALUE="Reset" id="form-reset">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <INPUT TYPE="SUBMIT" VALUE="Login" id="form-submit">
                                </TD> 
                            </TR>
                            <TR>
                                <% if (request.getAttribute("message") != null) {%>
                                  <TD colspan="3">
                                      <FONT color="${msgColor}">* &nbsp;<%=request.getAttribute("message")%></FONT>
                                  </TD>  
                                <%} else {%>
                                    <TD colspan="3">&nbsp;</TD>  
                                <%}%>
                            </TR>

                        </FORM>
                    </TABLE>      
                 </article>
               <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>

