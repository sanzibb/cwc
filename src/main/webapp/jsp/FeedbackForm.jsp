<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<!--jsp:useBean id="user" scope="session" class="com.aks.cwc.model.User"/-->
<jsp:useBean id="newFeedback" scope="request" class="com.aks.cwc.model.Feedback">
    <jsp:setProperty name="newFeedback" property="*"/>
</jsp:useBean>
<%
    String cssUrl = (String) application.getAttribute("cssUrl");   
    String action = request.getParameter("action");   
    if (action != null && action.equals("insertFeedback")) {  
        if (Util.isBlank(newFeedback.getSubject())) {
            request.setAttribute("message", "Subject can not be blank.");
        } else if (Util.isBlank(newFeedback.getDescription())) {
            request.setAttribute("message", "Message can not be blank.");
        } else if (newFeedback.getSubject().length()>45) {
            request.setAttribute("message", "Subject is too long. Maximum number of characters allowed is 45.");
        } else if (newFeedback.getDescription().length()>512) {
            request.setAttribute("message", "Message is too long. Maximum number of characters allowed is 512.");
        } else if (request.getSession()== null || request.getSession().getAttribute("user") == null){
            request.setAttribute("message", "You need to login to submit your feeback.");
        } else { 
            User user = (User) request.getSession().getAttribute("user");
            newFeedback.setUserId(user.getUserId());  
            newFeedback.setDate(new Date());
            if (dbBean.insertFeedback(newFeedback)) {
                 request.getParameterMap().clear();
                    request.setAttribute("status", "success");
                    request.setAttribute("message", "Thanks for your valuable feedback.");
                    RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/Confirmed.jsp");
                    requestDispatcher.forward(request, response);
                } else {
                    request.setAttribute("status", "error");
                    request.setAttribute("message", "Your feedback could not be submitted. Please try again.");
            }
        }

    }

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->

                    <h3>Feedback Form</h3>                   
                    <FORM method="post" action="ControllerServlet"> 
                        <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0">
                            <TR> 
                            <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <INPUT TYPE="HIDDEN" NAME="action" VALUE="insertFeedback"/> 
                            <TR> 
                                <TD colspan="5" id="form-sub-header">Feedback Details</TD>                                    
                            </TR>
                            <TR>
                                <TD ALIGN="LEFT">Subject</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <INPUT TYPE="TEXT" NAME="subject" VALUE="${param.subject}">
                                </TD>                        
                                               
                            </TR>
                            <TR>
                                <TD ALIGN="LEFT">Message</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <textarea NAME="description" rows="8" style="width: 90%">${param.description}</textarea>
                                </TD>                                    
                            </TR>
                            <TR> 
                                <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <TR>                                    
                                <TD ALIGN="RIGHT" colspan="5"><INPUT TYPE="RESET" VALUE="Reset" id="form-reset">&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="SUBMIT" VALUE="Submit" id="form-submit"></TD> 
                            </TR>
                            <TR> 
                                <TD colspan="5">(Administrator reserves right to remove or edit any improper words or sentences.)</TD>                                    
                            </TR>
                            <TR>
                                <% if (request.getAttribute("message") != null) {%>
                                <TD ALIGN="LEFT" colspan="5"><FONT color="red">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                                    <%} else {%>
                                <TD ALIGN="LEFT" colspan="5">&nbsp;</TD>  
                                    <%}%>
                            </TR>                            
                        </TABLE> 
                    </FORM>
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>

