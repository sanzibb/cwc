<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<%
    Object obj = session.getAttribute("user");
    User user = obj instanceof User ? (User) obj : null;
   
    String base = (String) application.getAttribute("base");
    String cssUrl = (String) application.getAttribute("cssUrl");
    
    List<Feedback> feedbacks = dbBean.getFeedbacks();
    
    List<Feedback> feedbacksByUser = new ArrayList<Feedback>();
    
    List<Feedback> feedbacksByOthers = new ArrayList<Feedback>();
    
    for(int i=0; i<feedbacks.size(); i++){
        Feedback co= feedbacks.get(i);
        if(user!=null && co.getUserId()==user.getUserId()){
            feedbacksByUser.add(co);
        }else{
            feedbacksByOthers.add(co);
        }
    }
    
    
    
    request.setAttribute("feedbacks", feedbacks);
    request.setAttribute("feedbacksByUser", feedbacksByUser);
    request.setAttribute("feedbacksByOthers", feedbacksByOthers);
   
    
%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                    <!-- page content starts here -->
                     <h3>Recent Feedbacks</h3>
                     <br>
                       <% if(user!=null && feedbacksByUser.size() !=0){ System.out.println("feedbackByUser: "+feedbacksByUser);%> 
                       <h5>Feedbacks submitted by you</h5>
                       <br>
                       <c:forEach items="${feedbacksByUser}" var="feedback" varStatus="counter">
                            <article>
                                <h4>${feedback.subject}</h4>
                                <div class="article-info">
                                    Posted on <strong>${feedback.date}</strong>&nbsp;
                                    by <strong>${dbBean.getUser(feedback.userId).getName()}</strong>&nbsp;                                   
                                </div>
                                ${feedback.description}                                
                            </article>
                        </c:forEach>         
                       
                       <%}%>
                       <% if(user!=null && feedbacksByOthers.size() !=0){System.out.println("feedbacksByOthers "+feedbacksByOthers);%> 
                       <h5>Feedbacks submitted by others</h5>
                       <br>
                       <c:forEach items="${feedbacksByOthers}" var="feedback" varStatus="counter">
                            <article>
                                <h4>${feedback.subject}</h4>
                                <div class="article-info">
                                    Posted on <strong>${feedback.date}</strong>&nbsp;
                                    by <strong>${dbBean.getUser(feedback.userId).getName()}</strong>&nbsp;                                   
                                </div>
                                ${feedback.description}                                 
                            </article>
                        </c:forEach>         
                       
                       <%}%>
                       <%if(user==null){%> 
                        <c:forEach items="${feedbacks}" var="feedback" varStatus="counter">
                            <article>
                                <h4>${feedback.subject}</h4>
                                <div class="article-info">
                                    Posted on <strong>${feedback.date}</strong>&nbsp;
                                    by <strong>${dbBean.getUser(feedback.userId).getName()}</strong>&nbsp;                                  
                                </div>
                                ${feedback.description}                                
                            </article>
                        </c:forEach> 
                        <%}%>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
