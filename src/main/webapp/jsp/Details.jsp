<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<%
    String cssUrl = (String) application.getAttribute("cssUrl");    
    int complaintId = Integer.parseInt(request.getParameter("complaintId"));
    Complaint complaint = dbBean.getComplaint(complaintId);
    complaint.setStatus(request.getParameter("status"));
    request.setAttribute("complaint", complaint);
    
    List<Activity> activities = dbBean.getActivities(complaintId);
    request.setAttribute("activities", activities);
    
    Activity lastActivity = activities.get(0);
    request.setAttribute("lastActivity", lastActivity);
    
    Map<Integer, String> userIdNameMap = dbBean.getUserIdNameMap();
    request.setAttribute("userIdNameMap", userIdNameMap);
        

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <!-- page content starts here -->
                <article>
                    <h3>${complaint.subject}</h3>
                    <div class="article-info">
                        Posted on <strong>${complaint.date}</strong>&nbsp;
                        by <strong>${dbBean.getUser(complaint.userId).getName()}</strong>&nbsp;
                        from <strong>${complaint.place},&nbsp;${complaint.city}</strong>
                       
                    </div>
                    <p> 
                        ${complaint.description} 
                        <br><strong>Status:</strong>&nbsp;${lastActivity.status}
                        <br>
                        <span style="float: left"><strong>Comments:</strong></span>
                        <span style="float: right"><strong>by</strong>&nbsp;${userIdNameMap[lastActivity.updatedBy]}</span>
                        <blockquote> ${lastActivity.status}</blockquote>
                    <p>    
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
