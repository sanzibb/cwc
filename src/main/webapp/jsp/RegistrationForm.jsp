<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="newUser" scope="request" class="com.aks.cwc.model.User">
    <jsp:setProperty name="newUser" property="*"/>
</jsp:useBean>
<%
    
    String formType = request.getParameter("formType");  
    
    if(formType.equals("user")){
       pageContext.setAttribute("formType", "user");
       pageContext.setAttribute("title", "User Registration Form");
    } 
    if(formType.equals("admin")){
       pageContext.setAttribute("formType", "admin"); 
       pageContext.setAttribute("title", "Administrator Registration Form");
    }  
  
    String cssUrl = (String) application.getAttribute("cssUrl");
    Map<Integer, Category> categoryMap = null;
    if(formType.equals("admin")){
        categoryMap = dbBean.getCategories("Admin", "SuperAdmin");
    }else{
        categoryMap = dbBean.getCategories("General");
    } 
    
    Collection<Category> categories = categoryMap.values();
    request.setAttribute("categories", categories);
    request.setAttribute("categoryMap", categoryMap);

    String action = request.getParameter("action");

    String maleChecked = "", femaleChecked = "";

    if (action != null && action.equals("insertUser")) {
        if (newUser.getCategoryId() > 0) {
            request.setAttribute("selectedCategoryId", newUser.getCategoryId());
        }
        if (!Util.isBlank(newUser.getGender())) {
            if (newUser.getGender().equalsIgnoreCase("male")) {
                maleChecked = "checked='checked'";
            } else if (newUser.getGender().equalsIgnoreCase("female")) {
                femaleChecked = "checked='checked'";
            }
            request.setAttribute("maleChecked", maleChecked);
            request.setAttribute("femaleChecked", femaleChecked);
        }

        //validations
        if (Util.isBlank(newUser.getUsername())) {
            request.setAttribute("message", "Username can not be blank.");
        } else if (Util.isBlank(newUser.getPassword())) {
            request.setAttribute("message", "Password can not be blank.");
        } else if (Util.isBlank(request.getParameter("retypePassword"))) {
            request.setAttribute("message", "Please retype your password.");
        } else if (!newUser.getPassword().equals(request.getParameter("retypePassword"))) {
            request.setAttribute("message", "Passwords mismatch. Please retype your password.");
        } else if (Util.isBlank(newUser.getName())) {
            request.setAttribute("message", "Name can not be blank.");
        } else if (Util.isBlank(newUser.getGender())) {
            request.setAttribute("message", "Please select your gender");
        } else if (Util.isBlank(newUser.getAddress())) {
            request.setAttribute("message", "Adderss can not be blank.");
        } else if (Util.isBlank(newUser.getPhone())) {
            request.setAttribute("message", "Phone can not be blank.");
        } else if (!Util.isValidPhone(newUser.getPhone())) {
            request.setAttribute("message", "Invalid phone number!");
        } else if (Util.isBlank(newUser.getEmail())) {
            request.setAttribute("message", "Email can not be blank.");
        } else if (!Util.isValidEmail(newUser.getEmail())) {
            request.setAttribute("message", "Invalid email id!");
        } else if (dbBean.usernameExists(newUser.getUsername())) {
            request.setAttribute("message", "Username already exists! Please choose another one.");
        } else if (dbBean.insertUser(newUser)) {
            request.getParameterMap().clear();
            request.setAttribute("status", "success");
            request.setAttribute("message", "Thanks for your registration. Please login now.");
            RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/LoginForm.jsp");
            requestDispatcher.forward(request, response);
        } else {
            request.setAttribute("message", "Registration failed. Please try again.");
        }

    }

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->
                    <h3>${title}</h3>
                    <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0">
                        <TR> 
                            <TD colspan="5">&nbsp;</TD>                                    
                        </TR>                            
                        <FORM method="post" action="ControllerServlet"> 
                            <INPUT TYPE="HIDDEN" NAME="action" VALUE="insertUser"/>
                            <INPUT TYPE="HIDDEN" NAME="formType" VALUE="${formType}"/>                                   
                            <TR> 
                                <TD colspan="5" id="form-sub-header">Login Details</TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Username</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <INPUT TYPE="TEXT" NAME="username" VALUE="${param.username}">
                                </TD>                                                              
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Password</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <INPUT TYPE="PASSWORD" NAME="password" VALUE="${param.password}">
                                </TD>                                                              
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Retype Password</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <INPUT TYPE="PASSWORD" NAME="retypePassword" VALUE="${param.retypePassword}">
                                </TD>                                                              
                            </TR>                               
                            <TR> 
                                <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <TR> 
                                <TD colspan="5" id="form-sub-header">Your Details</TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Name</TD>                                
                                <TD ALIGN="LEFT" colspan=4">
                                    <INPUT TYPE="TEXT" NAME="name" VALUE="${param.name}">
                                </TD>                 
                            </TR>                                
                            <TR> 
                                <TD ALIGN="LEFT">Gender</TD> 
                                <TD ALIGN="LEFT" colspan="4">                            
                                    <input type="radio" name="gender" value="male" <%=request.getAttribute("maleChecked")%> >Male
                                    <input type="radio" name="gender" value="female" <%=request.getAttribute("femaleChecked")%> >Female                                       
                                </TD>                                   
                            </TR>
                            
                            <TR>
                                 <% if (pageContext.getAttribute("formType").toString().equals("admin")) {%>
                                <TD ALIGN="LEFT">Designation</TD>
                                 <% } else { %>
                                 <TD ALIGN="LEFT">Category</TD>
                                <%}%>
                                <TD ALIGN="LEFT" colspan="4">                                   
                                    <select name="categoryId">
                                        <c:forEach items="${categories}" var="category">
                                            <c:choose>
                                                <c:when test="${not empty selectedCategoryId && selectedCategoryId eq category.categoryId}">
                                                    <option value="${category.categoryId}" selected="true">${category.name}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${category.categoryId}">${category.name}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </TD>                                    
                            </TR
                          
                            <TR> 
                                <TD ALIGN="LEFT">Address</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <textarea NAME="address" rows="2" style="width: 90%">${param.address}</textarea>
                                </TD>                                     
                            </TR> 
                           <TR> 
                                <TD ALIGN="LEFT">Phone</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="phone" VALUE="${param.phone}">
                                </TD> 
                                <TD>&nbsp;</TD>
                                <TD ALIGN="LEFT">Email</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="email" VALUE="${param.email}">
                                </TD>                                  
                            </TR>
                            <TR> 
                                <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <TR>                                    
                                <TD ALIGN="RIGHT" colspan="5">
                                    <INPUT TYPE="RESET" VALUE="Reset" id="form-reset">
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <INPUT TYPE="SUBMIT" VALUE="Register" id="form-submit">
                                </TD> 
                            </TR>
                            <TR>
                                <% if (request.getAttribute("message") != null) {%>
                                <TD colspan="5"><FONT color="red">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                                    <%} else {%>
                                <TD colspan="5">&nbsp;</TD>  
                                    <%}%>
                            </TR>
                        </FORM>
                    </TABLE>        
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
