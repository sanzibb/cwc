<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="user" scope="session" class="com.aks.cwc.model.User"/>
<jsp:useBean id="updatedUser" scope="request" class="com.aks.cwc.model.User">
    <jsp:setProperty name="updatedUser" property="*"/>
</jsp:useBean>
<%
    String cssUrl = (String) application.getAttribute("cssUrl");
    User dbUser = dbBean.getUser(user.getUserId());
    request.setAttribute("dbUser", dbUser);
    request.setAttribute("selectedCategoryId", dbUser.getCategoryId());

    String maleChecked = "", femaleChecked = "";

    maleChecked = dbUser.getGender().equalsIgnoreCase("male") ? "checked='checked'" : "";
    femaleChecked = dbUser.getGender().equalsIgnoreCase("female") ? "checked='checked'" : "";

    request.setAttribute("maleChecked", maleChecked);
    request.setAttribute("femaleChecked", femaleChecked);

    Map<Integer, Category> categoryMap = null;
    if(dbBean.getCategory(dbUser.getCategoryId()).isGeneral()){
        categoryMap = dbBean.getCategories("General");
    }else if(dbBean.getCategory(dbUser.getCategoryId()).isSuperAdmin()){
        categoryMap = dbBean.getCategories("Admin", "SuperAdmin");
    }else{
        categoryMap = dbBean.getCategories("Admin");
    }
    
    
    Collection<Category> categories = categoryMap.values();
    request.setAttribute("categories", categories);
    //request.setAttribute("categoryMap", categoryMap);

    String action = request.getParameter("action");

    if (action != null && action.equals("doUpdateProfile")) {
        if (updatedUser.getCategoryId() > 0) {
            request.setAttribute("selectedCategoryId", updatedUser.getCategoryId());
        }
        if (!Util.isBlank(updatedUser.getGender())) {
            maleChecked = updatedUser.getGender().equalsIgnoreCase("male") ? "checked='checked'" : "";
            femaleChecked = updatedUser.getGender().equalsIgnoreCase("female") ? "checked='checked'" : "";
            request.setAttribute("maleChecked", maleChecked);
            request.setAttribute("femaleChecked", femaleChecked);
        }

        if (Util.isBlank(updatedUser.getName())) {
            request.setAttribute("message", "Name can not be blank.");
        } else if (Util.isBlank(updatedUser.getGender())) {
            request.setAttribute("message", "Please select your gender");
        } else if (Util.isBlank(updatedUser.getAddress())) {
            request.setAttribute("message", "Adderss can not be blank.");
        } else if (Util.isBlank(updatedUser.getPhone())) {
            request.setAttribute("message", "Phone can not be blank.");
        } else if (!Util.isValidPhone(updatedUser.getPhone())) {
            request.setAttribute("message", "Invalid phone number!");
        } else if (Util.isBlank(updatedUser.getEmail())) {
            request.setAttribute("message", "Email can not be blank.");
        } else if (!Util.isValidEmail(updatedUser.getEmail())) {
            request.setAttribute("message", "Invalid email id!");
        } else if (Util.isBlank(updatedUser.getPassword())) {
            request.setAttribute("message", "Please type your existing password.");
        } else if (!updatedUser.isPasswordEqual(dbUser)) {
            request.setAttribute("message", "Wrong password!");
        } else if (dbUser.isProfileEqual(updatedUser)) {
            request.setAttribute("status", "success");
            request.setAttribute("message", "No change detected! Nothing to update.");
        } else {
            if (dbBean.updateUser(updatedUser)) {
                session.setAttribute("user", dbBean.getUser(dbUser.getUserId()));
                request.getParameterMap().clear();
                request.setAttribute("status", "success");
                request.setAttribute("message", "Your profile has been updated successfully");
                RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/Confirmed.jsp");
                requestDispatcher.forward(request, response);
            } else {
                request.setAttribute("message", "Your profile could not be updated. Please try again.");
            }
        }

    }

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->
                    <h3>Update Profile</h3>
                    <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  width="100%" height="100%">
                        <TR> 
                            <TD colspan="5">&nbsp;</TD>                                    
                        </TR>                            
                        <FORM method="post" action="ControllerServlet"> 
                            <INPUT TYPE="HIDDEN" NAME="action" VALUE="doUpdateProfile"/>  
                            <INPUT TYPE="HIDDEN" NAME="userId" VALUE="${dbUser.userId}"/> 
                            <TR> 
                                <TD colspan="5" id="form-sub-header"><b>Your Details</b></TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Name</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <INPUT TYPE="TEXT" NAME="name" VALUE="${param.name!=null?param.name:dbUser.name}">
                                </TD>                
                            </TR>                                
                            <TR> 
                                <TD ALIGN="LEFT">Gender</TD>
                                <TD ALIGN="LEFT" colspan="4">                          

                                    <input type="radio" name="gender" value="male" <%=request.getAttribute("maleChecked")%> >Male
                                    <input type="radio" name="gender" value="female" <%=request.getAttribute("femaleChecked")%> >Female                                       
                                </TD>                                   
                            </TR>
                            <TR>
                                <TD ALIGN="LEFT">Category</TD>
                                <TD ALIGN="LEFT" colspan="4">                                    
                                    <select name="categoryId">
                                        <c:forEach items="${categories}" var="category">
                                            <c:choose>
                                                <c:when test="${not empty selectedCategoryId && selectedCategoryId eq category.categoryId}">
                                                    <option value="${category.categoryId}" selected="true">${category.name}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${category.categoryId}">${category.name}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Address</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <textarea NAME="address" rows="2" style="width: 90%">${param.address != null?param.address:dbUser.address}</textarea>
                                </TD>                                     
                            </TR> 
                            <TR> 
                            <TR> 
                                <TD ALIGN="LEFT">Phone</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="phone" VALUE="${param.phone!=null?param.phone:dbUser.phone}">
                                </TD> 
                                <TD>&nbsp;</TD>
                                <TD ALIGN="LEFT">Email</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="email" VALUE="${param.email!=null?param.email:dbUser.email}">
                                </TD>                                  
                            </TR>
                            <TR> 
                                <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <TR> 
                                <TD colspan="5" id="form-sub-header">Authentication</TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Password</TD>
                                <TD ALIGN="LEFT" colspan="4"><INPUT TYPE="PASSWORD" NAME="password" VALUE="${param.password}"></TD>                                                              
                            </TR>
                            <TR> 
                                <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <TR>                                    
                                <TD ALIGN="RIGHT" colspan="5">                                        
                                   <INPUT TYPE="SUBMIT" VALUE="Update" id="form-submit">
                                </TD> 
                            </TR>
                            <TR>
                                <% if (request.getAttribute("message") != null) {%>
                                <TD colspan="5"><FONT color="red">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                                    <%} else {%>
                                <TD colspan="5">&nbsp;</TD>  
                                    <%}%>
                            </TR>
                        </FORM>                       
                    </TABLE>        
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
