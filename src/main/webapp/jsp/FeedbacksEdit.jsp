<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="newFeedback" scope="request" class="com.aks.cwc.model.Feedback">
    <jsp:setProperty name="newFeedback" property="*"/>
</jsp:useBean>
<%
    Object obj = session.getAttribute("user");
    User user = obj instanceof User ? (User) obj : null;
   
    String base = (String) application.getAttribute("base");
    String cssUrl = (String) application.getAttribute("cssUrl");
    
    List<Feedback> feedbacks = dbBean.getFeedbacks();
    request.setAttribute("feedbacks", feedbacks);
    
    String action = request.getParameter("action");
    
    String feedbackId = request.getParameter("feedbackId");
   
    if (action != null && action.equals("editFeedback") && feedbackId!=null) {
        
        if (Util.isBlank(newFeedback.getSubject())) {
            request.setAttribute("message", "Subject can not be blank.");
        } else if (Util.isBlank(newFeedback.getDescription())) {
            request.setAttribute("message", "Message can not be blank.");
        } else if (newFeedback.getSubject().length()>45) {
            request.setAttribute("message", "Subject is too long. Maximum number of characters allowed is 45.");
        } else if (newFeedback.getDescription().length()>512) {
            request.setAttribute("message", "Message is too long. Maximum number of characters allowed is 512.");
        } else {            
            if (dbBean.editFeedback(newFeedback)) {
                 request.getParameterMap().clear();
                    request.setAttribute("status", "success");
                    request.setAttribute("message", "Changes in feedback have been saved.");
                    RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/Confirmed.jsp");
                    requestDispatcher.forward(request, response);
                } else {
                    request.setAttribute("status", "error");
                    request.setAttribute("message", "Changes could not be saved. Please try again.");
            }
           }
       } 

    

   
   
    
%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                    <!-- page content starts here -->
                     <h3>Edit Feedbacks</h3>
                     <br>              
                       
                       <%if(user!=null){%> 
                        <c:forEach items="${feedbacks}" var="feedback" varStatus="counter">
                            <article>
                            <div class="article-info">
                             Posted on <strong>${feedback.date}</strong>&nbsp; 
                             by <strong>${dbBean.getUser(feedback.userId).getName()}</strong>&nbsp;
                             from <strong>${dbBean.getUser(feedback.userId).getAddress()}</strong>
                            </div>
                            </article>
                            <FORM method="post" action="ControllerServlet">
                                <INPUT TYPE="HIDDEN" NAME="action" VALUE="editFeedback"/>
                                <INPUT TYPE="HIDDEN" NAME="feedbackId" VALUE="${feedback.feedbackId}"/> 
                                <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0">                              
                                    <TR>
                                        <TD ALIGN="LEFT">Subject</TD>
                                        <TD ALIGN="LEFT" colspan="4"><INPUT TYPE="TEXT" NAME="subject" VALUE="${(param.subject!=null && param.feedbackId==feedback.feedbackId)? param.subject:feedback.subject}"></TD>                                    
                                    </TR>
                                    <TR>
                                        <TD ALIGN="LEFT">Message</TD>
                                        <TD ALIGN="LEFT" colspan="4"><textarea NAME="description" rows="2" style="width: 100%">${(param.description!=null && param.feedbackId==feedback.feedbackId)? param.description:feedback.description}</textarea> </TD>                                  
                                    </TR>
                                    <TR>                                    
                                        <TD ALIGN="RIGHT" colspan="5"><INPUT TYPE="SUBMIT" VALUE="Edit" id="form-submit"></TD> 
                                    </TR>
                                    <c:choose>
                                        <c:when test="${not empty requestScope.message && param.feedbackId==feedback.feedbackId}">
                                            <TR><TD ALIGN="LEFT" colspan="5"><FONT color="red">* &nbsp;<%=request.getAttribute("message")%></FONT></TD></TR>                                          
                                        </c:when>                                   
                                    </c:choose>                                    
                                </TABLE>
                            </FORM>
                        </c:forEach> 
                        <%}%>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
