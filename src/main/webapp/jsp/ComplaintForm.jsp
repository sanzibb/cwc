<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="user" scope="session" class="com.aks.cwc.model.User"/>
<jsp:useBean id="newComplaint" scope="request" class="com.aks.cwc.model.Complaint">
    <jsp:setProperty name="newComplaint" property="*"/>
</jsp:useBean>
<%
    String cssUrl = (String) application.getAttribute("cssUrl");
    Map<Integer, Identity> identitiesMap = dbBean.getIdentities();
    Collection<Identity> identities = identitiesMap.values();

    request.setAttribute("identitiesMap", identitiesMap);
    request.setAttribute("identities", identities);

    String action = request.getParameter("action");
    String maleChecked = "", femaleChecked = "";
    
    maleChecked = user.getGender().equalsIgnoreCase("male") ? "checked='checked'" : "";
    femaleChecked = user.getGender().equalsIgnoreCase("female") ? "checked='checked'" : "";

    request.setAttribute("maleChecked", maleChecked);
    request.setAttribute("femaleChecked", femaleChecked);

    if (action != null && action.equals("insertComplaint")) {       
         
         newComplaint.setUserId(user.getUserId());
        
        
        if (newComplaint.getIdentityId() > 0) {
            request.setAttribute("selectedIdentityId", newComplaint.getIdentityId());
        }
        
        Date date = Util.getValidDate(newComplaint.getDateStr());
        newComplaint.setDate(date);
        //validations
        if (Util.isBlank(newComplaint.getSubject())) {
            request.setAttribute("message", "Subject can not be blank.");
        } else if (Util.isBlank(newComplaint.getDateStr())) {
            request.setAttribute("message", "Date can not be blank.");
        } else if (newComplaint.getDate() == null) {
            request.setAttribute("message", "Please enter date in YYYY-MM-DD format");
        } else if (Util.isBlank(newComplaint.getDescription())) {
            request.setAttribute("message", "Description can not be blank.");
        } else if (Util.isBlank(newComplaint.getPlace())) {
            request.setAttribute("message", "Place can not be blank.");
        } else if (Util.isBlank(newComplaint.getCity())) {
            request.setAttribute("message", "City can not be blank.");
        } else if (Util.isBlank(newComplaint.getIdentityNumber())) {
            request.setAttribute("message", "ID Number can not be blank.");
        } else {
            int newComplaintId = dbBean.getLastComplaintId() + 1;
            newComplaint.setComplaintId(newComplaintId);
            if (dbBean.insertComplaint(newComplaint)) {
                Activity activity = new Activity();
                activity.setComplaintId(newComplaintId);
                activity.setReporterId(1);
                activity.setAssigneeId(1);
                activity.setStatus("PENDING");
                activity.setComment("");
                activity.setUpdatedBy(1);
                activity.setDate(new java.util.Date());
                if (dbBean.insertActivity(activity)) {
                    request.getParameterMap().clear();
                    request.setAttribute("status", "success");
                    request.setAttribute("message", "Thanks for lodging your complaint. Your compliaint number is " + newComplaintId + ".");
                    RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/Confirmed.jsp");
                    requestDispatcher.forward(request, response);
                } else {
                    request.setAttribute("status", "error");
                    request.setAttribute("message", "Complaints could not be submitted. Please try again.");
                }
            }

        }

    }

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->

                    <h3>Complaints Form</h3>
                    <FORM method="post" action="ControllerServlet"> 
                        <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0">
                            <TR> 
                            <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <INPUT TYPE="HIDDEN" NAME="action" VALUE="insertComplaint"/> 
                            <TR> 
                                <TD colspan="5" id="form-sub-header">Complaint Details</TD>                                    
                            </TR>
                            <TR>
                                <TD ALIGN="LEFT">Subject</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="subject" VALUE="${param.subject}">
                                </TD>                                                              
                                <TD>&nbsp;</TD>
                                <TD ALIGN="LEFT">Date</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="dateStr" placeholder="YYYY-MM-DD" VALUE="${param.dateStr}">
                                </TD>                   
                            </TR>
                            <TR>
                                <TD ALIGN="LEFT">Description</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <textarea NAME="description" rows="4" style="width: 90%">${param.description}</textarea>
                                </TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Place</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="place" VALUE="${param.place}">
                                </TD>                                    
                                <TD>&nbsp;</TD>
                                <TD ALIGN="LEFT">City</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="city" VALUE="${param.city}">
                                </TD>                                    
                            </TR>
                            <TR> 
                                <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <TR> 
                                <TD colspan="5" id="form-sub-header">Your Details</TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Name</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <INPUT TYPE="TEXT" NAME="name" disabled="true" VALUE="${user.name}">
                                </TD>                  
                            </TR>
                            <TR>
                                <TD ALIGN="LEFT">Gender</TD>
                                <TD ALIGN="LEFT" colspan="4">                                
                                    <input type="radio" name="gender" disabled="true" value="male" <%=request.getAttribute("maleChecked")%> >Male
                                    <input type="radio" name="gender" disabled="true" value="female" <%=request.getAttribute("femaleChecked")%> >Female                                        
                                </TD>                                   
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Address</TD>
                                <TD ALIGN="LEFT" colspan="4">
                                    <textarea NAME="address" disabled="true" rows="2" style="width: 90%">${user.address}</textarea>
                                </TD>                                     
                            </TR> 
                            <TR>
                                <TD ALIGN="LEFT">ID Number</TD>
                                <TD ALIGN="LEFT">                                                                      
                                    <INPUT TYPE="TEXT" NAME="identityNumber" VALUE="${param.identityNumber}">
                                </TD> 
                                <TD>&nbsp;</TD>
                                <TD ALIGN="LEFT">ID Type</TD>
                                <TD ALIGN="LEFT">                                  
                                    <select name="identityId">
                                        <c:forEach items="${identities}" var="identity">
                                            <c:choose>
                                                <c:when test="${not empty selectedIdentityId && selectedIdentityId eq identity.identityId}">
                                                    <option value="${identity.identityId}" selected="true">${identity.name}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${identity.identityId}">${identity.name}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
                                </TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Phone</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="phone" disabled="true" VALUE="${user.phone}">
                                </TD> 
                                <TD>&nbsp;</TD>
                                <TD ALIGN="LEFT">Email</TD>
                                <TD ALIGN="LEFT">
                                    <INPUT TYPE="TEXT" NAME="email" disabled="true" VALUE="${user.email}">
                                </TD>                                  
                            </TR>
                            <TR> 
                                <TD colspan="5">&nbsp;</TD>                                    
                            </TR>
                            <TR>                                    
                                <TD ALIGN="RIGHT" colspan="5"><INPUT TYPE="RESET" VALUE="Reset" id="form-reset">&nbsp;&nbsp;&nbsp;&nbsp;<INPUT TYPE="SUBMIT" VALUE="Submit" id="form-submit"></TD> 
                            </TR>
                            <TR>
                                <% if (request.getAttribute("message") != null) {%>
                                <TD ALIGN="LEFT" colspan="5"><FONT color="red">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                                    <%} else {%>
                                <TD ALIGN="LEFT" colspan="5">&nbsp;</TD>  
                                    <%}%>
                            </TR>
                        </TABLE> 
                    </FORM>
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>

