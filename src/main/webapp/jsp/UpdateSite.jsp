<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="keyValue" scope="application" class="java.util.HashMap"/>
 <%   
         String cssUrl = (String) application.getAttribute("cssUrl");
         String action = request.getParameter("action");         
         
         String title = request.getParameter("title");
         if(title != null){
           pageContext.setAttribute("title", title);
         }
         
         String key = request.getParameter("key");
         if(key != null){
           pageContext.setAttribute("key", key);
         }
        
         if (action != null && action.equals("doUpdateSite")) {
            String textAreaValue = request.getParameter("textAreaValue");           
            if(dbBean.updateKeyValue(key, textAreaValue)){
                keyValue.put(key, textAreaValue);
                String location = key.equals("CONTACTS")?"/jsp/Contacts.jsp":key.equals("FAQ")?"/jsp/Faq.jsp":"/jsp/AboutUs.jsp";
                RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(location);
                requestDispatcher.forward(request, response);
            }else{
                request.setAttribute("status", "error");
                request.setAttribute("message", "Contacts could not be saved. Please try again.");
            }  
            
        }
 %>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <link type="text/css" rel="stylesheet" href="../script/jquery-te-1.4.0.css">
        <script type="text/javascript" src="../script//jquery.min.js" charset="utf-8"></script>
        <script type="text/javascript" src="../script/jquery-te-1.4.0.min.js" charset="utf-8"></script> 
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->
                    <h3>${title}</h3> 
                    <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  width="100%" height="100%">
                            
                            <TR>
                                <TD colspan="3" style="text-align: left">
                                    <FORM  id="target" method="post" action="ControllerServlet">
                                        <INPUT TYPE="HIDDEN" NAME="action" VALUE="doUpdateSite"/>
                                        <INPUT TYPE="HIDDEN" NAME="key" VALUE="${key}"/>                                        
                                        <textarea cols="50" rows="7" name="textAreaValue" class="jqte-test">
                                            ${keyValue[key].toString().trim()}
                                        </textarea>
                                        <div style="text-align: right"><input type="submit" value="Save"></div>
                                    </FORM>  
                                </TD>

                            </TR>
                            <TR>
                                <% if (request.getAttribute("message") != null) {%>
                                <TD colspan="3"><FONT color="red">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                                    <%} else {%>
                                <TD colspan="3">&nbsp;</TD>  
                                    <%}%>
                            </TR>
                        </TABLE>        
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
        <script>
            $('.jqte-test').jqte();

            // settings of status
            var jqteStatus = true;
            $(".status").click(function()
            {
                jqteStatus = jqteStatus ? false : true;
                $('.jqte-test').jqte({"status": jqteStatus});
            });
           
        </script>   
    </body>
</html>
       