<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="user" scope="session" class="com.aks.cwc.model.User"/>
<%
    String cssUrl = (String) application.getAttribute("cssUrl");
    String password = request.getParameter("password");
    String newPassword = request.getParameter("newPassword");
    String retypeNewPassword = request.getParameter("retypeNewPassword");
    
    String action = request.getParameter("action");
    
    if (action != null && action.equals("doChangePassword")) {
        if (Util.isBlank(password)) {
            request.setAttribute("status", "error");
            request.setAttribute("message", "Please type your existing password.");
        } else if (Util.isBlank(newPassword)) {
            request.setAttribute("status", "error");
            request.setAttribute("message", "Please type your new password.");
        } else if (Util.isBlank(retypeNewPassword)) {
            request.setAttribute("status", "error");
            request.setAttribute("message", "Please re-type your new password.");
        } else if (!newPassword.equals(retypeNewPassword)) {
            request.setAttribute("status", "error");
            request.setAttribute("message", "Passwords mismatch! Please re-type your new password again.");
        } else if (!password.equals(user.getPassword())) {
            request.setAttribute("status", "error");
            request.setAttribute("message", "Your existing password is not valid.");
        } else {
            if (dbBean.updatePassword(user.getUserId(), newPassword)) {
                request.getParameterMap().clear();
                user.setPassword(newPassword);
                request.setAttribute("status", "success");
                request.setAttribute("message", "Your password has been successfully changed.");
                RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/Confirmed.jsp");
                requestDispatcher.forward(request, response);
            } else {
                request.setAttribute("status", "error");
                request.setAttribute("message", "Your password could not be changed. Please try again.");                
            }            
        }
    }

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->
                    <h3>Change Password</h3>   
                    <hr>                   
                    <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  style="width:360px">

                        <FORM method="post" action="ControllerServlet"> 
                            <INPUT TYPE="HIDDEN" NAME="action" VALUE="doChangePassword"/>                             

                            <% if (request.getAttribute("status") != null && request.getAttribute("status").toString().equalsIgnoreCase("SUCCESS") && request.getAttribute("message") != null) {%>
                            <TR>
                                <TD colspan="3" ALIGN="CENTER"><FONT color="green">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                            </TR>
                            <%} else {%> 
                            <TR> 
                                <TD colspan="3" ALIGN="CENTER">&nbsp;</TD>                                    
                            </TR>
                            <%}%>                                    
                            <TR> 
                                <TD ALIGN="LEFT">Old Password</TD>
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="PASSWORD" NAME="password" VALUE="${param.password}">
                                </TD>                                                              
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">New Password</TD>
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="PASSWORD" NAME="newPassword" VALUE="${param.newPassword}">
                                </TD>                                                              
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT">Retype New Password</TD>
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="PASSWORD" NAME="retypeNewPassword" VALUE="${param.retypeNewPassword}">
                                </TD>                                                              
                            </TR>                                
                            <TR>                                    
                                <TD ALIGN="LEFT">&nbsp;</TD>
                                <TD ALIGN="LEFT" colspan="2">
                                    <INPUT TYPE="SUBMIT" VALUE="Change Password" id="form-submit">
                                </TD> 
                            </TR>
                            <TR>
                                <% if (request.getAttribute("status") != null && request.getAttribute("status").toString().equalsIgnoreCase("ERROR") && request.getAttribute("message") != null) {%>
                                <TD colspan="3"><FONT color="red">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                                    <%} else {%>
                                <TD colspan="3">&nbsp;</TD>  
                                    <%}%>
                            </TR>
                        </FORM>
                    </TABLE>        
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
