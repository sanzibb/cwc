<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="user" scope="session" class="com.aks.cwc.model.User"/>
<jsp:useBean id="newActivity" scope="request" class="com.aks.cwc.model.Activity">
    <jsp:setProperty name="newActivity" property="*"/>    
</jsp:useBean>

<%
    String cssUrl = (String) application.getAttribute("cssUrl");
    int complaintId = Integer.parseInt(request.getParameter("complaintId"));
    Complaint complaint = dbBean.getComplaint(complaintId);
    request.setAttribute("complaint", complaint);

    List<Activity> activities = dbBean.getActivities(complaintId);
    request.setAttribute("activities", activities);
    
    Activity lastActivity = activities.get(0);
    request.setAttribute("lastActivity", lastActivity);

    Map<Integer, String> userIdNameMap = dbBean.getUserIdNameMap();
    Collection<Integer> userIdSet = userIdNameMap.keySet();
    request.setAttribute("userIdSet", userIdSet);

    Map<String, Status> statusMap = dbBean.getStatuses();
    Collection<String> statusSet = statusMap.keySet();
    request.setAttribute("statusSet", statusSet);

    request.setAttribute("userIdNameMap", userIdNameMap);

    Map<Integer, Identity> identitiesMap = dbBean.getIdentities();
    Collection<Identity> identities = identitiesMap.values();

    request.setAttribute("identitiesMap", identitiesMap);
    request.setAttribute("identities", identities);

    String action = request.getParameter("action");

    if (action != null && action.equals("updateActivity")) {

        if (dbBean.insertActivity(newActivity)) {

            complaint = dbBean.getComplaint(complaintId);
            request.setAttribute("complaint", complaint);

            activities = dbBean.getActivities(complaintId);
            request.setAttribute("activities", activities);

            request.setAttribute("color", "green");
            request.setAttribute("message", "Activity has been saved.");
        } else {
            request.setAttribute("color", "red");
            request.setAttribute("message", "Activity could not be saved. Please try again.");
        }
    }
    boolean isAssEnabled = (dbBean.getCategory(user.getCategoryId()).isSuperAdmin() || lastActivity.getReporterId()==user.getUserId())?true:false;
    
    boolean isRepoEnabled = dbBean.getCategory(user.getCategoryId()).isSuperAdmin()?true:false;
    
    request.setAttribute("isAssEnabled", isAssEnabled);
    request.setAttribute("isRepoEnabled", isRepoEnabled);

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->
                    <h3>Complaint Details</h3> 
                    <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  width="100%" height="100%">
                            <TR> 
                                <TD colspan="3">&nbsp;</TD>                                    
                            </TR>                            
                            <TR> 
                                <TD ALIGN="LEFT">Subject&nbsp;&nbsp;${complaint.subject}</TD>                                                              
                                <TD>&nbsp;</TD>
                                <TD ALIGN="RIGHT">Date&nbsp;&nbsp;${complaint.date}</TD>                   
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT" colspan="3">Description&nbsp;&nbsp;${complaint.description}</TD>                                    
                            </TR>

                            <TR> 
                                <TD ALIGN="LEFT">Place&nbsp;&nbsp;${complaint.place}</TD>                                    
                                <TD>&nbsp;</TD> 
                                <TD ALIGN="RIGHT">City&nbsp;&nbsp;${complaint.city}</TD>                                    
                            </TR>
                            <TR> 
                                <TD colspan="3">&nbsp;</TD>                                    
                            </TR>
                            <TR> 
                                <TD colspan="3" BGCOLOR="F6F6F6">Person Details</TD>                                    
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT" colspan="3">Name&nbsp;&nbsp;${dbBean.getUser(complaint.userId).getName()}</TD>                  

                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT" colspan="3">Gender&nbsp;&nbsp;${dbBean.getUser(complaint.userId).getGender()}</TD>                                   
                            </TR>
                            <TR> 
                                <TD ALIGN="LEFT" colspan="3">Address&nbsp;&nbsp;${dbBean.getUser(complaint.userId).getAddress()}</TD>                                     

                            </TR> 
                            <TR> 
                                <TD ALIGN="LEFT">
                                    ID Number&nbsp;&nbsp;${complaint.identityNumber}                                    

                                </TD> 
                                <TD>&nbsp;</TD>
                                <TD ALIGN="RIGHT">ID Type&nbsp;&nbsp;${identitiesMap[complaint.identityId].name}                                    

                                </TD>                                    

                            </TR>

                            <TR> 
                                <TD ALIGN="LEFT">Phone&nbsp;&nbsp;${dbBean.getUser(complaint.userId).getPhone()}</TD> 
                                <TD>&nbsp;</TD>
                                <TD ALIGN="RIGHT">Email&nbsp;&nbsp;${dbBean.getUser(complaint.userId).getEmail()}</TD>                                  
                            </TR>
                        </TABLE>                            
                        <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  width="100%" height="100%" id="box">
                            <FORM method="post" action="ControllerServlet">
                                <INPUT TYPE="HIDDEN" NAME="action" VALUE="updateActivity"/>
                                <INPUT TYPE="HIDDEN" NAME="complaintId" VALUE="${complaint.complaintId}"/>

                                <INPUT TYPE="HIDDEN" NAME="updatedBy" VALUE="${user.userId}"/>

                                <TR>                                    
                                    <TD ALIGN="LEFT">Assignee: &nbsp;&nbsp;
                                        <% if(isAssEnabled){%>
                                        <select name="assigneeId">
                                            <c:forEach items="${userIdSet}" var="userId">
                                                <c:choose>
                                                    <c:when test="${userId eq activities[0].assigneeId}">
                                                        <option value="${userId}" selected="true">${userIdNameMap[userId]}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${userId}">${userIdNameMap[userId]}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                        <%}else{%>
                                         <INPUT TYPE="HIDDEN" NAME="assigneeId" VALUE="${lastActivity.assigneeId}"/>
                                         ${userIdNameMap[lastActivity.assigneeId]}
                                        <%}%>
                                    </TD>
                                    <TD>&nbsp;</TD>

                                    <TD ALIGN="RIGHT">Reporter: &nbsp;&nbsp;
                                      <% if(isRepoEnabled){%>
                                        <select name="reporterId">
                                            <c:forEach items="${userIdSet}" var="userId">
                                                <c:choose>
                                                    <c:when test="${userId eq activities[0].reporterId}">
                                                        <option value="${userId}" selected="true">${userIdNameMap[userId]}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${userId}">${userIdNameMap[userId]}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                        <%}else{%>
                                         <INPUT TYPE="HIDDEN" NAME="reporterId" VALUE="${lastActivity.reporterId}"/>
                                         ${userIdNameMap[lastActivity.reporterId]}
                                        <%}%>
                                    </TD>    

                                </TR>
                                <TR>                                    
                                    <TD ALIGN="LEFT" colspan="3">Status: &nbsp;&nbsp;                                    
                                        <select name="status">
                                            <c:forEach items="${statusSet}" var="status">
                                                <c:choose>
                                                    <c:when test="${status eq activities[0].status}">
                                                        <option value="${status}" selected="true">${status}</option>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <option value="${status}">${status}</option>
                                                    </c:otherwise>
                                                </c:choose>
                                            </c:forEach>
                                        </select>
                                    </TD>
                                </TR>

                                <TR> 
                                    <TD ALIGN="LEFT" colspan="3" ALIGN="TOP">Comment:&nbsp;&nbsp;<textarea NAME="comment" rows="3" cols="60"></textarea></TD>                                     

                                </TR>
                                <TR>                                    
                                    <TD ALIGN="RIGHT" colspan="3"><INPUT TYPE="SUBMIT" VALUE="Update"></TD> 
                                </TR>


                            </FORM>
                        </TABLE> 
                        <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  width="100%" height="100%">
                            <TR> 
                                <TD colspan="3" ALIGN="LEFT"> <h4>Past Activities: </h4></TD>                                    
                            </TR>

                            <c:forEach items="${activities}" var="activity" varStatus="counter">

                                <TR> 
                                    <TD ALIGN="LEFT"><FONT FACE="Verdana" SIZE="2">${activity.date}</FONT></TD>
                                    <TD ALIGN="LEFT"><FONT FACE="Verdana" SIZE="2">&nbsp;</FONT></TD>
                                    <TD ALIGN="RIGHT"><FONT FACE="Verdana" SIZE="2">Updated By: ${userIdNameMap[activity.updatedBy]}</FONT></TD>
                                </TR>
                                <TR> 
                                    <TD ALIGN="LEFT"><FONT FACE="Verdana" SIZE="2">Assignee: ${userIdNameMap[activity.assigneeId]}</FONT></TD>
                                    <TD ALIGN="LEFT"><FONT FACE="Verdana" SIZE="2">&nbsp;</FONT></TD>
                                    <TD ALIGN="RIGHT"><FONT FACE="Verdana" SIZE="2">Reporter: ${userIdNameMap[activity.reporterId]}</FONT></TD>
                                </TR> 


                                <TR> 
                                    <TD ALIGN="LEFT" colspan="3"><FONT FACE="Verdana" SIZE="2">Status: ${activity.status}</FONT></TD>                                       

                                </TR>
                                <TR> 
                                    <TD ALIGN="LEFT" colspan="3"><FONT FACE="Verdana" SIZE="2">Comment: ${activity.comment}</FONT></TD>                                       

                                </TR>                                
                                <c:if test="${!counter.last}">
                                    <TR> 
                                        <TD colspan="3"><FONT FACE="Verdana" SIZE="2">&nbsp;<hr>&nbsp;</FONT></TD>                                        

                                    </TR> 
                                </c:if>

                            </c:forEach>     
                            <TR>
                                <% if (request.getAttribute("message") != null) {%>
                                <TD colspan="3"><FONT color="${color}">* &nbsp;<%=request.getAttribute("message")%></FONT></TD>  
                                    <%} else {%>
                                <TD colspan="3">&nbsp;</TD>  
                                    <%}%>
                            </TR>

                        </TABLE>        
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>