<%@ page import="com.aks.cwc.model.*" %> 
<%
    String base = (String) application.getAttribute("base");
    String imageUrl = (String) application.getAttribute("imageUrl");
    Object obj = session.getAttribute("user");
    User user = obj instanceof User ? (User) obj : null;
%>
<link rel="shortcut icon" href="http://sstatic.net/stackoverflow/img/favicon.ico">
<div id="sitename">
    <div class="width">
        <!--h1><a href="#">city without crimes</a></h1-->
        <nav style="float: left">
            <ul>
                <li><a href="<%=base%>?action=displayHome">Home</a></li>
                <li class=""><a href="<%=base%>?action=displayAboutUs">About Us</a></li>
                <li><a href="<%=base%>?action=displaySearch">Search</a></li>
                <li><a href="<%=base%>?action=displayFaq">FAQ</a></li>
                <li><a href="<%=base%>?action=displayContacts">Contacts</a></li>                
             </ul>
        </nav>
        <nav style="float: right">
            <ul>
                <% if (user == null) {%>
                <li><a  style="cursor: default;font-weight:bold;" href="#">Welcome Guest</a></li>
                <li><a href="<%=base%>?action=displayLogin">Login</a></li>  
                    <%} else {%>
                <li><a  style="cursor: default; font-weight:bold;" href="#">Welcome ${user.name}</a></li>
                <li><a href="<%=base%>?action=doLogout">Logout</a></li>      
                    <%}%>           
            </ul>
        </nav>    
        <div class="clear"></div>
    </div>
</div>
<header style="padding:0px 0 0px;">
    <div class="width">
        <img src="../images/banner.jpg" alt="city without crime" height="150" width="100%">
        <!--
        <h2>city without crime</h2>
        <div class="tagline">
            <p>While every crime violates the law, not every violation of the law counts as a crime.</p>
        </div> -->
    </div>
</header>


