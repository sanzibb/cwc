<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>
<jsp:useBean id="user" scope="session" class="com.aks.cwc.model.User"/>
<%  
    String base = (String) application.getAttribute("base");
    String cssUrl = (String) application.getAttribute("cssUrl");  
    
    List<User> users = dbBean.getUsers();    
    request.setAttribute("users", users); 

    String action = request.getParameter("action");
    String userId = request.getParameter("userId");
    String status = request.getParameter("status");
    String name = request.getParameter("name");
    
    if (action != null && action.equals("changeStatus")) {
        dbBean.updateUserStatus(Integer.parseInt(userId),status);
        request.getParameterMap().clear();
        request.setAttribute("status", "success");
        if(status.equalsIgnoreCase("NORMAL")){
             request.setAttribute("message", "The user '" + name + "' has been restricted to login.");
        }else{
             request.setAttribute("message", "The user '" + name + "' has been allowed to login.");
        }
       
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher("/jsp/Confirmed.jsp");
        requestDispatcher.forward(request, response);
    }
    
%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                    <!-- page content starts here -->
                     <h3>Users</h3>
                     <br>    

                     <c:forEach items="${users}" var="userObj" varStatus="counter">
                        <article>
                            <c:choose>
                                <c:when test="${userObj.userId eq user.userId}">  
                                     <h4>${userObj.name}&nbsp;(you)</h4>
                                </c:when>
                                <c:otherwise>
                                     <h4>${userObj.name}</h4>
                                </c:otherwise>     
                            </c:choose> 
                            <div class="article-info">                               
                                Address:&nbsp; <strong>${userObj.address}</strong> <br>                               
                                Phone:&nbsp; <strong>${userObj.phone}</strong> &nbsp;&nbsp;&nbsp; 
                                Email:&nbsp; <strong>${userObj.email}</strong> <br> 
                                <c:choose>
                                   <c:when test="${not dbBean.getCategory(userObj.categoryId).isGeneral()}">
                                       Designation:&nbsp; <strong>${dbBean.getCategory(userObj.categoryId).getName()}</strong> &nbsp; 
                                       <strong>(${dbBean.getCategory(userObj.categoryId).getType()})</strong><br>                                        
                                    </c:when>                                   
                                </c:choose> 
                                <c:choose>
                                   <c:when test="${userObj.isLoginAllowed()}">       
                                        Login:&nbsp; <strong>ALLOWED</strong>&nbsp; 
                                   </c:when>
                                   <c:otherwise>
                                        Login:&nbsp; <strong>RESTRICTED</strong>&nbsp; 
                                   </c:otherwise>     
                                </c:choose> 
                            </div> 
                            <c:choose>
                                <c:when test="${userObj.userId ne user.userId}">                                    
                                   <span style="float: right">
                                      <a href="<%=base%>?action=changeStatus&userId=${userObj.userId}&status=${userObj.status}&name=${userObj.name}" class="button">${userObj.getCommandText()}</a>
                                   </span> 
                              </c:when>                                   
                            </c:choose> 
                        </article>                             
                    </c:forEach> 
                       
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>
