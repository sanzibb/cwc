<%@ page import="com.aks.cwc.model.*" %> 
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="dbBean" scope="application" class="com.aks.cwc.model.DbBean"/>

<%
    String cssUrl = (String) application.getAttribute("cssUrl");
    String keyword = request.getParameter("keyword");
    int complaintId = 0;
    if(request.getParameter("complaintId")!=null){
        try{
            complaintId = Integer.parseInt(request.getParameter("complaintId").trim());
        }catch(Exception e){
            
        }
    }
    Collection<Complaint> complaints = dbBean.getComplaints(complaintId,keyword );
    request.setAttribute("complaints", complaints);

%> 
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title>city without crime</title>
        <link rel="stylesheet" href="<%=(cssUrl + "styles.css")%>" type="text/css" />
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!--
        afflatus, a free CSS web template by ZyPOP (zypopwebtemplates.com/)
        
        Download: http://zypopwebtemplates.com/
        
        License: Creative Commons Attribution
        //-->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    </head>
    <body>
        <jsp:include page="Header.jsp" flush="true"/>
        <section id="body" class="width clear">
            <jsp:include page="Menu.jsp" flush="true"/>
            <section id="content" class="column-right">
                <article>                
                    <!-- page content starts here -->
                    <h3>Search Result</h3> 
                       <TABLE CELLPADDING="5" CELLSPACING="0" BORDER="0"  width="100%" height="100%">
                           
                            <TR> 
                                 <TD colspan="3">&nbsp;</TD>                                    
                            </TR>
                            <c:if test="${empty complaints}">
                                   <TR> 
                                       <TD colspan="3"><FONT FACE="Verdana" SIZE="2">No record found matching search criteria.</FONT></TD>                                        
                                   </TR> 
                            </c:if>
                            <c:forEach items="${complaints}" var="complaint" varStatus="counter">
                                <TR> 
                                    <TD ALIGN="LEFT"><FONT FACE="Verdana" SIZE="2">${complaint.date}</FONT>&nbsp;${complaint.subject}</TD> 
                                    <TD><FONT FACE="Verdana" SIZE="2">&nbsp;</FONT></TD> 
                                    <TD ALIGN="RIGHT"><FONT FACE="Verdana" SIZE="2">posted by: ${dbBean.getUser(complaint.userId).getName()}</FONT></TD> 
                                </TR>
                                <TR> 
                                    <TD colspan="3"><FONT FACE="Verdana" SIZE="2">${complaint.description}</FONT></TD>                                        

                                </TR>
                                <TR> 
                                    <TD><FONT FACE="Verdana" SIZE="2">&nbsp;</TD> 
                                    <TD><FONT FACE="Verdana" SIZE="2"></FONT>&nbsp;</TD> 
                                    <TD ALIGN="RIGHT"><FONT FACE="Verdana" SIZE="2">status: ${complaint.status}</FONT></TD> 
                                </TR>
                                <c:if test="${!counter.last}">
                                   <TR> 
                                       <TD colspan="3"><FONT FACE="Verdana" SIZE="2">&nbsp;<hr>&nbsp;</FONT></TD>                                        
                                   </TR> 
                                </c:if>
                        </c:forEach>                          
                      </TABLE>        
                </article>
                <!-- page content ends here -->
            </section>
        </section>
        <jsp:include page="Footer.jsp" flush="true"/>
    </body>
</html>