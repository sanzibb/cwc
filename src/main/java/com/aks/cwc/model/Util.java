/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.aks.cwc.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Sanjeev
 */
public class Util {
    
    private static final Logger LOGGER = Logger.getLogger(Util.class.getName());
    
    private static final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String DATE_PATTERN = "^[0-9]{4}-[0-9]{2}-[0-9]{2}$"; 
    
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    public static boolean isBlank(String str){
        if(str==null){
            return true;
        }else{
            return str.trim().isEmpty();
        }
    }
    
    public static boolean isValidPhone(String str){
        return str.matches("[0-9]{10}");
    }
    
    public static boolean isValidEmail(String str){
        return str.matches(EMAIL_PATTERN);
    }   
    
    public static Date getValidDate(String str) {
        if(isBlank(str)){
            return null;
        }else if (str.matches(DATE_PATTERN)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setLenient(false);           
            try {
                Date date = sdf.parse(str);
                LOGGER.log(Level.INFO, "The date [{0}] is valid.", date.toString());
                return date;
            } catch (ParseException ex) {
                LOGGER.log(Level.WARNING, str+" is not valid date", ex);
            }
        }
        LOGGER.log(Level.INFO, "The date [{0}] is not valid.", str);
        return null;
    }
    
    /**
     * Returns date in "yyyy-MM-dd HH:mm:ss" format
     * @param date
     * @return 
     */
    public static String getDateString(Date date){
        return dateFormat.format(date);
    }     
    
}
