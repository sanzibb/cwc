/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.aks.cwc.model;

/**
 *
 * @author Sanjeev
 */
public class Category {
    private int categoryId;
    private String name;
    private String type;

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    } 
    
    public boolean isAdmin(){
        return type.equalsIgnoreCase("Admin");
    }
    
    public boolean isSuperAdmin(){
        return type.equalsIgnoreCase("SuperAdmin");
    }
    
    public boolean isGeneral(){
        return type.equalsIgnoreCase("General");
    }
    
}
