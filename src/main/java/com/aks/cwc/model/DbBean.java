/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aks.cwc.model;

import java.util.ArrayList;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbBean {

    private String dbUrl;
    private String dbUserName;
    private String dbPassword;
    private static final Logger LOGGER = Logger.getLogger(DbBean.class.getName());

    public void setDbUrl(String url) {
        dbUrl = url;
    }

    public void setDbUserName(String userName) {
        dbUserName = userName;
    }

    public void setDbPassword(String password) {
        dbPassword = password;
    }   
    
     /**
     * to be browsed by general public
     * @param args userId optional
     * @return
     */
    public List<Complaint> getComplaints(int... args) {
        String sqlPart = "";
        if(args.length > 0){
            int userId = args[0];
            sqlPart =  " AND (a1.assignee_id ="+userId+" OR a1.reporter_id ="+userId+") ";
        }
        List<Complaint> complaints = new ArrayList<Complaint>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT DISTINCT c.complaint_id, c.identity_id, c.identity_number, "
                    + " c.complaint_user_id, c.date, c.place, "
                    + " c.city, c.subject, c.description, a.status FROM CWC_COMPLAINT c, "
                    + " CWC_ACTIVITY a WHERE  c.complaint_id = a.complaint_id AND "
                    + " a.date = (SELECT max(a1.date) from CWC_ACTIVITY a1 "
                    + " WHERE a1.complaint_id = a.complaint_id "+sqlPart+") "
                    + " ORDER BY c.date DESC";
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Complaint complaint = new Complaint();
                complaint.setComplaintId(rs.getInt("complaint_id"));
                complaint.setIdentityId(rs.getInt("identity_id"));
                complaint.setIdentityNumber(rs.getString("identity_number"));               
                complaint.setDate(rs.getDate("date"));
                complaint.setPlace(rs.getString("place"));
                complaint.setCity(rs.getString("city"));
                complaint.setSubject(rs.getString("subject"));
                complaint.setDescription(rs.getString("description"));
                complaint.setStatus(rs.getString("status"));
                complaint.setUserId(rs.getInt("complaint_user_id"));
                complaints.add(complaint);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        System.out.println("complaints.size(): "+complaints.size());
        return complaints;
    }    
    
    public List<Activity> getActivities(int complaintId) {
        List<Activity>  activities = new ArrayList<Activity>();        
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT activity_id, complaint_id, reporter_id, assignee_id,"
                    + " comment, date, status, updated_by from CWC_ACTIVITY "
                    + " WHERE complaint_id ="+complaintId+" order by date desc";
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            while(rs.next()) {
               Activity  activity = new Activity();
                activity.setActivityId(rs.getInt("activity_id"));
                activity.setComplaintId(rs.getInt("complaint_id"));
                activity.setReporterId(rs.getInt("reporter_id"));
                activity.setAssigneeId(rs.getInt("assignee_id"));
                activity.setComment(rs.getString("comment"));
                activity.setDate(rs.getTimestamp("date"));
                activity.setStatus(rs.getString("status"));
                activity.setUpdatedBy(rs.getInt("updated_by"));
                activities.add(activity);                
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
       
        return activities;
    }
    
    
    
     public Complaint getComplaint(int complaintId) {
        Complaint complaint = null;
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT c.complaint_id, c.identity_id, c.identity_number, "
                    + " c.complaint_user_id, c.date, c.place, "
                    + " c.city, c.subject, c.description FROM CWC_COMPLAINT c "
                    + " WHERE c.complaint_id = "+complaintId;
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            if(rs.next()) {
                complaint = new Complaint();
                complaint.setComplaintId(rs.getInt("complaint_id"));
                complaint.setIdentityId(rs.getInt("identity_id"));
                complaint.setIdentityNumber(rs.getString("identity_number"));
                //complaint.setName(rs.getString("name"));
                //complaint.setGender(rs.getString("gender"));
                //complaint.setAddress(rs.getString("address"));
                //complaint.setPhone(rs.getString("phone"));
                //complaint.setEmail(rs.getString("email"));
                complaint.setDate(rs.getDate("date"));
                complaint.setPlace(rs.getString("place"));
                complaint.setCity(rs.getString("city"));
                complaint.setSubject(rs.getString("subject"));
                complaint.setDescription(rs.getString("description"));
                complaint.setUserId(rs.getInt("complaint_user_id"));                
                
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
       
        return complaint;
    }
    
    /**
     *
     * @return
     */
    public Map<Integer, Identity> getIdentities() {
        Map<Integer, Identity> identities = new LinkedHashMap<Integer, Identity>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT identity_id, name FROM CWC_IDENTITY ";
            LOGGER.info(sql);       
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Identity identity = new Identity();
                identity.setIdentityId(rs.getInt("identity_id"));                
                identity.setName(rs.getString("name"));               
                identities.put(identity.getIdentityId(), identity);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return identities;
    }
    
     /**
     *
     * @return
     */
    public Map<Integer, String> getOfficerNameMap() {
        Map<Integer, String> officerNameMap = new LinkedHashMap<Integer, String>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT officer_id, name FROM CWC_OFFICER ";
            LOGGER.info(sql);       
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {                             
                officerNameMap.put(rs.getInt("officer_id"), rs.getString("name"));
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return officerNameMap;
    }
    
    
     /**
     *
     * @return
     */
    public Map<Integer, String> getUserIdNameMap() {
        Map<Integer, String> userIdNameMap = new LinkedHashMap<Integer, String>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT user_id, name FROM CWC_USER ";
            LOGGER.info(sql);       
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {                             
                userIdNameMap.put(rs.getInt("user_id"), rs.getString("name"));
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return userIdNameMap;
    }
    
     /**
     *
     * @return
     */
    public Map<String, Status> getStatuses() {
        Map<String, Status> statuses = new LinkedHashMap<String, Status>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT name, description FROM CWC_STATUS ";
            LOGGER.info(sql);       
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Status status = new Status();
                status.setName(rs.getString("name"));                
                status.setDescription(rs.getString("description"));               
                statuses.put(status.getName(), status);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return statuses;
    }  
   
    
     /**
     *
     * @return
     */
    public Map<Integer, Category> getCategories() {
        Map<Integer, Category>  categories = new LinkedHashMap<Integer, Category>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT category_id, name, type FROM CWC_CATEGORY ";
            LOGGER.info(sql);       
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("category_id"));                
                category.setName(rs.getString("name"));
                category.setType(rs.getString("type"));
                categories.put(category.getCategoryId(), category);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return categories;
    }
    
     /**
     *
     * @param types
     * @return
     */
    public Map<Integer, Category> getCategories(String... types) {
        
        String whereClause ="";
        if(types.length>0){            
           String inClause="";
           for(int i=0; i<types.length; i++){          
                inClause =inClause+" '"+types[i]+"'";
                if(i!=types.length-1){
                    inClause =inClause+",";
                }
           }
           whereClause =whereClause+" WHERE type IN ("+inClause+")";
        }        
      
        Map<Integer, Category>  categories = new LinkedHashMap<Integer, Category>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT category_id, name, type FROM CWC_CATEGORY "+whereClause;
            LOGGER.info(sql);       
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt("category_id"));                
                category.setName(rs.getString("name"));
                category.setType(rs.getString("type"));
                categories.put(category.getCategoryId(), category);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return categories;
    }  
    
    public List<Complaint> getComplaintsByUserId(int userId) {
        List<Complaint> complaints = new ArrayList<Complaint>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT complaint_id, indentity_id, indentity_number, "
                    + "complaint_user_id, date, place, "
                    + "city, subject, description, status FROM CWC_COMPLAINT c, CWC_OFFICER_COMPLAINT oc "
                    + "WHERE c.complaint_id = oc.complaint_id AND oc.officerId = "
                    + userId + " ORDER BY date";
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Complaint complaint = new Complaint();
                complaint.setComplaintId(rs.getInt("complaint_id"));
                complaint.setIdentityId(rs.getInt("indentity_id"));
                complaint.setIdentityNumber(rs.getString("indentity_number"));
                //complaint.setName(rs.getString("name"));
                //complaint.setGender(rs.getString("gender"));
                //complaint.setAddress(rs.getString("address"));
                //complaint.setPhone(rs.getString("phone"));
                //complaint.setEmail(rs.getString("email"));
                complaint.setDate(rs.getDate("date"));
                complaint.setPlace(rs.getString("place"));
                complaint.setCity(rs.getString("city"));
                complaint.setSubject(rs.getString("subject"));
                complaint.setDescription(rs.getString("description"));
                complaint.setStatus(rs.getString("status"));
                complaint.setUserId(rs.getInt("complaint_user_id"));
                complaints.add(complaint);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return complaints;
    }
    
     public Collection<Complaint> getComplaints(int complaintId, String keyword) {
        String whereClause = complaintId > 0 ? " WHERE ( complaint_id = "+complaintId+") ":"";
        if(Util.isBlank(whereClause) && Util.isBlank(keyword)){
            return new ArrayList<Complaint>();
        }
        if(Util.isBlank(whereClause)){
           whereClause += " WHERE "; 
        }else if(!Util.isBlank(keyword)){
           whereClause += " OR ";  
        }
        if(!Util.isBlank(keyword)){
        whereClause += " ( subject like '%"+keyword+"%') OR  ( description like '%"+keyword+"%') ";
        }
        
        Set<Complaint> complaints = new LinkedHashSet<Complaint>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT complaint_id, identity_id, identity_number, "
                    + "complaint_user_id, date, place, "
                    + "city, subject, description, status FROM CWC_COMPLAINT "
                    +whereClause;
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Complaint complaint = new Complaint();
                complaint.setComplaintId(rs.getInt("complaint_id"));
                complaint.setIdentityId(rs.getInt("identity_id"));
                complaint.setIdentityNumber(rs.getString("identity_number"));
                //complaint.setName(rs.getString("name"));
                //complaint.setGender(rs.getString("gender"));
                //complaint.setAddress(rs.getString("address"));
                //complaint.setPhone(rs.getString("phone"));
               // complaint.setEmail(rs.getString("email"));
                complaint.setDate(rs.getDate("date"));
                complaint.setPlace(rs.getString("place"));
                complaint.setCity(rs.getString("city"));
                complaint.setSubject(rs.getString("subject"));
                complaint.setDescription(rs.getString("description"));
                complaint.setStatus(rs.getString("status"));
                complaint.setUserId(rs.getInt("complaint_user_id"));
                complaints.add(complaint);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return complaints;
    }    
    
    public Category getCategory(int categoryId) {
        Category category = null;
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT category_id, name, type FROM CWC_CATEGORY "
                    + " WHERE category_id="+categoryId;                    
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                category = new Category();
                category.setCategoryId(rs.getInt("category_id"));                
                category.setName(rs.getString("name"));
                category.setType(rs.getString("type"));               
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return category;
    }

    /**
     * Returns true if the Complaint could be inserted into database
     *
     * @param complaint - instance of Complaint to be inserted  
     * @return true if the Complaint could be inserted into database
     */
    public boolean insertComplaint(Complaint complaint) {

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "INSERT INTO CWC_COMPLAINT"
                    + " (complaint_id, identity_id, identity_number, "
                    + " date, place, city, subject, description, complaint_user_id)"
                    + " VALUES"
                    + " (" + complaint.getComplaintId() + ", " + complaint.getIdentityId() + ",'" + complaint.getIdentityNumber() + "', "
                    + " '" + complaint.getDateStr() + "', '" + complaint.getPlace() + "', "
                    + " '" + complaint.getCity() + "', '" + complaint.getSubject() + "', '" + complaint.getDescription() + "', "+complaint.getUserId()+")";
            LOGGER.info(sql);
            s.executeUpdate(sql);
            s.close();           
            connection.commit();
            connection.close();
            returnValue = true;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Complaint could not be inserted into database: ", e);
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return returnValue;
    }    
   
    
    /**
    * 
    * @param officerId
    * @param newPassword
    * @return 
    */
    public boolean updatePassword(int userId, String newPassword) {

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "UPDATE CWC_USER "
                    + " SET password = '"+newPassword+"' "
                    + " WHERE user_id = "+userId;
            LOGGER.info(sql);
            s.executeUpdate(sql);
            s.close();            
            connection.commit();
            connection.close();
            returnValue = true;
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return returnValue;
    }
    
    /**
     * Returns true if the Complaint could be inserted into database
     *
     * @param activity - instance of Activity to be inserted
     * @return true if the Activity could be inserted into database
     */
    public boolean insertActivity(Activity activity) {

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "INSERT INTO CWC_ACTIVITY"
                    + " (complaint_id, reporter_id, assignee_id, comment, status, date, updated_by)"
                    + " VALUES"
                    + " (" + activity.getComplaintId() + ", " + activity.getReporterId() + ", " + activity.getAssigneeId()+ ",'" + activity.getComment() + "',"
                    + " '" + activity.getStatus() + "', '" + Util.getDateString(new java.util.Date()) + "', "+activity.getUpdatedBy()+")";
            LOGGER.info(sql);
            s.executeUpdate(sql);
            s.close();
            
            connection.commit();
            connection.close();
            returnValue = true;
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return returnValue;
    }
    
     public int getLastComplaintId() {
        int lastComplaintId = -1;

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "select max(complaint_id) from cwc_complaint";                   
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            if (rs.next()) {                
                lastComplaintId = rs.getInt(1);
            }
            s.close();
            connection.commit();
            connection.close();

        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return lastComplaintId;
    }   
    
     /**
     * Returns the instance of User having the username and password if found
     * in Database
     *
     * @param username - the Username of the User
     * @param password - the Password of the User
     * @return the instance of User having the username and password if found
     * in Database otherwise null
     */
    public User doLogin(String username, String password) {
        User user = null;

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "SELECT user_id, username, password, name, address,"
                    + " phone, email, gender, user_category_id, status FROM CWC_USER"
                    + " WHERE username='" + username + "' and password='" + password + "'";
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            if (rs.next()) {
                user = new User();
                user.setUserId(rs.getInt("user_id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                user.setAddress(rs.getString("address"));
                user.setPhone(rs.getString("phone"));
                user.setEmail(rs.getString("email"));
                user.setGender(rs.getString("gender"));
                user.setCategoryId(rs.getInt("user_category_id"));
                user.setStatus(rs.getString("status"));                
            }
            s.close();
            connection.commit();
            connection.close();

        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return user;
    }
    
    
    /**
     * Returns true if the Officer could be inserted into database
     *
     * @param user - instance of User to be inserted
     * @return true if the Officer could be inserted into database
     */
    public boolean insertUser(User user) { 
        
        final String STATUS = getCategory(user.getCategoryId()).isGeneral()?"NORMAL":"CANCEL";   

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "INSERT INTO CWC_USER"
                    + " (username, password, name, address, phone, email, gender, user_category_id, status)"
                    + " VALUES"
                    + " ('" + user.getUsername() + "','" + user.getPassword() + "','" + user.getName() + "',"
                    + " '" + user.getAddress() + "', '" + user.getPhone() + "', '" + user.getEmail() + "', "
                    + " '" + user.getGender() + "', " + user.getCategoryId() + ", '"+STATUS+"' )";
            LOGGER.info(sql);
            s.executeUpdate(sql);
            s.close();
            connection.commit();
            connection.close();
            returnValue = true;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE,"Error occurred while inserting user: ", e);
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        
        
        return returnValue;
    }
    
    
   
    
    
     /**
     * Updates User record on all fields except username and password
     * @param user
     * @return 
     */
    public boolean updateUser(User user) {

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "UPDATE CWC_USER "
                    + " SET name = '"+user.getName().trim()+"', "
                    + " address = '"+user.getAddress().trim()+"', "
                    + " user_category_id = "+user.getCategoryId()+", "
                    + " gender = '"+user.getGender().trim()+"', "
                    + " phone = '"+user.getPhone().trim()+"', "
                    + " email = '"+user.getEmail().trim()+"' "              
                    + " WHERE user_id = "+user.getUserId()+" "
                    + " AND password = '"+user.getPassword()+"' " ;
            LOGGER.info(sql);
            int rowCount = s.executeUpdate(sql);
            s.close();            
            connection.commit();
            connection.close();
            returnValue = rowCount>=1;
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return returnValue;
    } 
    
     public User getUser(int userId) {
        User user = null;
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT user_id, username, password, name, "
                    + " address, phone, email, gender, user_category_id, status "
                    + " FROM CWC_USER "
                    + " WHERE user_id = "
                    + userId;
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            if(rs.next()) {
                user = new User();
                user.setUserId(rs.getInt("user_id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));             
                user.setAddress(rs.getString("address"));
                user.setPhone(rs.getString("phone"));
                user.setEmail(rs.getString("email"));
                user.setGender(rs.getString("gender")); 
                user.setStatus(rs.getString("status")); 
                user.setCategoryId((rs.getInt("user_category_id")));               
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return user;
    }
     
    public List<User> getUsers() {
        List<User> users = new ArrayList<User>();        
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT user_id, username, password, name, "
                    + " address, phone, email, gender, user_category_id, status "
                    + " FROM CWC_USER ";
                   
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) { 
                User  user = new User();
                user.setUserId(rs.getInt("user_id"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));             
                user.setAddress(rs.getString("address"));
                user.setPhone(rs.getString("phone"));
                user.setEmail(rs.getString("email"));
                user.setGender(rs.getString("gender")); 
                user.setStatus(rs.getString("status")); 
                user.setCategoryId((rs.getInt("user_category_id"))); 
                users.add(user);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        LOGGER.log(Level.INFO, "total no. of users: {0}", users.size());
        return users;
    }    
    
    
    /**
     * Returns true if the username already exists in database otherwise false
     *
     * @param username - Username of an user
     * @return true if the username already exists in database otherwise false
     */
    public boolean usernameExists(String username) {
        boolean returnValue = false;

        int userId = -1;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "SELECT user_id FROM CWC_USER"
                    + " WHERE username='" + username + "'";
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            if (rs.next()) {
                userId = rs.getInt("user_id");
            }
            s.close();
            connection.commit();
            connection.close();
            returnValue = userId > 0;
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return returnValue;
    }
    
    /**
     *
     * @return
     */
    public Map<String, String> getKeyValues() {
        Map<String, String> map = new HashMap<String, String>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT key, value FROM CWC_KEYVALUE ";
            LOGGER.info(sql);       
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {              
                String key = rs.getString("key");                
                String value = rs.getString("value");               
                map.put(key, value);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println("exception: " + e);
        }
        return map;
    } 
    
    /**
     * Updates KEYVALUE table
     * @param key
     * @param value
     * @return 
     */
    public boolean updateKeyValue(String key, String value) {

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "UPDATE CWC_KEYVALUE "
                    + " SET value = '"+value+"' "                              
                    + " WHERE key = '"+key+"' ";                
            LOGGER.info(sql);
            int rowCount = s.executeUpdate(sql);
            s.close();            
            connection.commit();
            connection.close();
            returnValue = rowCount>=1;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Problem occured while updating CWC_KEYVALUE", e);
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                LOGGER.log(Level.SEVERE, "Problem occured while updating CWC_KEYVALUE and closing connection.", e);
            }
        }
        return returnValue;
    } 
    
    /**
    * 
    * @param userId
    * @return 
    */
    public boolean updateUserStatus(int userId, String currentStatus) {
        
        String updatedStatus = currentStatus.equalsIgnoreCase("NORMAL")?"CANCEL":"NORMAL";

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "UPDATE CWC_USER "
                    + " SET status = '"+updatedStatus+"' "
                    + " WHERE user_id = "+userId;
            LOGGER.info(sql);
            s.executeUpdate(sql);
            s.close();            
            connection.commit();
            connection.close();
            returnValue = true;
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                System.out.println("SQLException: " + se);
            }
        }
        return returnValue;
    }
    
     /**
     * Returns true if the Feedback could be inserted into database
     *
     * @param feedback - instance of Feedback to be inserted  
     * @return true if the Feedback could be inserted into database
     */
    public boolean insertFeedback(Feedback feedback) {
        boolean returnValue = false;
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "INSERT INTO CWC_FEEDBACK"
                    + " (date, subject, description, feedback_user_id)"
                    + " VALUES ('"+Util.getDateString(feedback.getDate())+ "', "
                    + " '" + feedback.getSubject() + "', "
                    + " '" + feedback.getDescription()+ "', "
                    + " "+feedback.getUserId() + ")";
            LOGGER.info(sql);
            s.executeUpdate(sql);
            s.close();           
            connection.commit();
            connection.close();
            returnValue = true;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Feedback could not be inserted into database: ", e);
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }
            } catch (SQLException se) {
                LOGGER.log(Level.SEVERE, "Database could not be rolled back or connection could not be closed: ", se);
            }
        }
        return returnValue;
    }

    /**
     * to be browsed by general public    
     * @return
     */
    public List<Feedback> getFeedbacks() {       
        List<Feedback> feedbacks = new ArrayList<Feedback>();
        try {
            Connection connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            Statement s = connection.createStatement();
            String sql = "SELECT  feedback_id, date, subject, description, feedback_user_id FROM cwc_feedback";                  
            LOGGER.info(sql);
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()) {
                Feedback feedback = new Feedback();
                feedback.setFeedbackId(rs.getInt("feedback_id"));                       
                feedback.setDate(rs.getDate("date"));               
                feedback.setSubject(rs.getString("subject"));
                feedback.setDescription(rs.getString("description"));               
                feedback.setUserId(rs.getInt("feedback_user_id"));
                feedbacks.add(feedback);
            }
            rs.close();
            s.close();
            connection.close();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Feedbacks could not be fetched from database: ", e);
        }
        LOGGER.log(Level.INFO, "count of Feedbacks : {0}", feedbacks.size());      
        return feedbacks;
    }

    /**
     * Edit User feedback on all fields except date
     * @param feedback
     * @return 
     */
    public boolean editFeedback(Feedback feedback) {

        boolean returnValue = false;

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
            connection.setAutoCommit(false);
            Statement s = connection.createStatement();
            String sql = "UPDATE CWC_FEEDBACK "
                    + " SET subject = '"+feedback.getSubject().trim()+"', "
                    + " description = '"+feedback.getDescription().trim()+"' "                           
                    + " WHERE feedback_id = "+feedback.getFeedbackId();                  
            LOGGER.info(sql);
            int rowCount = s.executeUpdate(sql);
            s.close();            
            connection.commit();
            connection.close();
            returnValue = rowCount>=1;
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Feedback could not be edited: ", e);
            try {
                if (connection != null) {
                    connection.rollback();
                    connection.close();
                }

            } catch (SQLException se) {
                LOGGER.log(Level.SEVERE, "Database could not be rolled back or connection could not be closed: ", se);
            }
        }
        return returnValue;
    }     
    
}
