/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.aks.cwc.model;

/**
 *
 * @author Aks
 */
public class User {
    private int userId;
    private String username;
    private String password;
    private String name;
    private String address;
    private String phone;
    private String email;
    private String gender;
    private int categoryId;
    private String status;    

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }   

    public String getUsername() {
        return Util.isBlank(username)?"":username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return Util.isBlank(password)?"":password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
         return Util.isBlank(name)?"":name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
       return Util.isBlank(address)?"":address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
       return Util.isBlank(phone)?"":phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return Util.isBlank(email)?"":email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return Util.isBlank(gender)?"":gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    } 
   
  
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.userId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        return this.userId == other.userId;
    }
    
    public boolean isProfileEqual(User other){
        if(this.getUserId()!=other.getUserId()){
            return false;
        }else if(!this.getName().equals(other.getName().trim())){
            return false;
        }else if(!this.getAddress().equals(other.getAddress().trim())){
            return false;
        }else if(!this.getGender().equalsIgnoreCase(other.getGender().trim())){
            return false;
        }else if(!this.getPhone().equalsIgnoreCase(other.getPhone().trim())){
            return false;
        }else if(!this.getEmail().equalsIgnoreCase(other.getEmail().trim())){
            return false;
        }else if(!this.getStatus().equalsIgnoreCase(other.getStatus().trim())){
            return false;
        }
        return true;
    }
    
    public boolean isPasswordEqual(User other){
        return this.getPassword().equals(other.getPassword());
    }
    
    public boolean isLoginAllowed(){
         return status.equalsIgnoreCase("NORMAL");   
    }
    
    public String getCommandText(){
         return isLoginAllowed()?"Restrict":"Allow";   
    }


    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", username=" + username + ", password=" + password + ", name=" + name + ", address=" + address + ", phone=" + phone + ", email=" + email + ", gender=" + gender +", categoryId=" + categoryId + ", status=" + status + '}';
    }        
    
}
