/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aks.cwc.controller;

import com.aks.cwc.model.DbBean;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;

public class ControllerServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(ControllerServlet.class.getName());

    /**
     * Initialize global variables
     *
     * @param config
     * @throws javax.servlet.ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {

        LOGGER.info("initializing controller servlet.");

        ServletContext context = config.getServletContext();
        String base = config.getInitParameter("base");
        String imageUrl = config.getInitParameter("imageUrl");
        String cssUrl = config.getInitParameter("cssUrl");

        LOGGER.log(Level.INFO, "base: {0}", base);
        LOGGER.log(Level.INFO, "imageUrl: {0}", imageUrl);

        context.setAttribute("base", base);
        context.setAttribute("imageUrl", imageUrl);
        context.setAttribute("cssUrl", cssUrl);

        // instantiating the DbBean 
        DbBean dbBean = new DbBean();

        String dbUrl = null;
        String dbUserName = null;
        String dbPassword = null;

        String databaseUrl = System.getenv("DATABASE_URL");

        if (databaseUrl == null || databaseUrl.isEmpty()) {
            LOGGER.log(Level.INFO, "Environment variable 'DATABASE_URL' not found. So using 'init-param' from web.xml");
            dbUrl = config.getInitParameter("dbUrl");
            dbUserName = config.getInitParameter("dbUserName");
            dbPassword = config.getInitParameter("dbPassword");
        } else {
            LOGGER.log(Level.INFO, "Environment variable 'DATABASE_URL' found.");
            try {
                URI dbUri = new URI(System.getenv("DATABASE_URL"));
                dbUserName = dbUri.getUserInfo().split(":")[0];
                dbPassword = dbUri.getUserInfo().split(":")[1];
                dbUrl = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();
            } catch (URISyntaxException ex) {
                LOGGER.log(Level.SEVERE, "Database connection could not be established.", ex);
            }

        }

        LOGGER.log(Level.INFO, "dbUrl: {0}", dbUrl);
        LOGGER.log(Level.INFO, "dbUserName: {0}", dbUserName);
        LOGGER.log(Level.INFO, "dbPassword: {0}", dbPassword);

        // initialize the DbBean's fields 
        dbBean.setDbUrl(dbUrl);
        dbBean.setDbUserName(dbUserName);
        dbBean.setDbPassword(dbPassword);

        // put the bean in the servlet context 
        // the bean will be accessed from JSP pages 
        context.setAttribute("dbBean", dbBean);

        try {
            // loading the database JDBC driver 
            String jdbcDriver = config.getInitParameter("jdbcDriver");
            Class.forName(jdbcDriver);
            LOGGER.log(Level.INFO, "JDBC Driver [{0}] has been loaded.", jdbcDriver);
        } catch (ClassNotFoundException e) {
            LOGGER.severe(e.toString());
        }

        Map<String, String> keyValue = dbBean.getKeyValues();
        context.setAttribute("keyValue", keyValue);
        super.init(config);
    }

    /**
     * Process the HTTP Get request
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        doPost(request, response);
    }

    /**
     * Process the HTTP Post request
     *
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws
            ServletException, IOException {
        String base = "/jsp/";
        String url = base + "Default.jsp";
        String action = request.getParameter("action");
        LOGGER.log(Level.INFO, "action= {0}", action);

        if (action != null) {
            if (action.equals("displayHome")) {
                url = base + "Default.jsp";
            } else if (action.equals("displayAboutUs")) {
                url = base + "AboutUs.jsp";
            } else if (action.equals("displaySearch")) {
                url = base + "SearchForm.jsp";
            } else if (action.equals("doSearch")) {
                url = base + "SearchResult.jsp";
            } else if (action.equals("displayFaq")) {
                url = base + "Faq.jsp";
            } else if (action.equals("displayContacts")) {
                url = base + "Contacts.jsp";
            } else if (action.equals("displayUpdateContacts")
                    || action.equals("displayUpdateFaq")
                    || action.equals("displayUpdateAboutUs")
                    || action.equals("doUpdateSite")) {
                url = base + "UpdateSite.jsp";
            } else if (action.equals("displayComplaintForm") || action.equals("insertComplaint")) {                
                 if (request.getSession() != null && request.getSession().getAttribute("user") != null) {
                    url = base + "ComplaintForm.jsp";
                } else {
                    request.setAttribute("nextURL", base +"ComplaintForm.jsp");
                    request.setAttribute("message", "You need to login to access Complaint Form.");
                    url = base + "LoginForm.jsp";                    
                }
            }else if (action.equals("changePassword") || action.equals("doChangePassword")) {
                url = base + "ChangePassword.jsp";
            } else if (action.equals("updateProfile") || action.equals("doUpdateProfile")) {
                url = base + "UpdateProfile.jsp";
            } else if (action.equals("displayLogin") || action.equals("doLogin")) {
                url = base + "LoginForm.jsp";
            } else if (action.equals("doLogout")) {
                request.getSession().invalidate();
                request.setAttribute("status", "success");
                request.setAttribute("message", "You have been logged out.");
                url = base + "LoginForm.jsp";
            } else if (action.equals("displayComplaintDetails") || action.equals("updateActivity")) {
                url = base + "ComplaintDetails.jsp";
            } else if (action.equals("displayReadMore")) {
                url = base + "Details.jsp";
            } else if (action.equals("displayRegistration") || action.equals("insertUser")) {                
                url = base + "RegistrationForm.jsp";
            } else if (action.equals("displayUsers") || action.equals("changeStatus")) {                
                url = base + "Users.jsp";
            } else if (action.equals("displayFeedbackForm") || action.equals("insertFeedback")) {
                url = base + "FeedbackForm.jsp";
            } else if (action.equals("displayFeedbacks")) {
                url = base + "Feedbacks.jsp";
            } else if (action.equals("displayEditFeedbacks") || action.equals("editFeedback") ) {
                url = base + "FeedbacksEdit.jsp";
            }
        }
        LOGGER.log(Level.INFO, "url: {0}", url);
        RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(url);
        requestDispatcher.forward(request, response);
    }

}
